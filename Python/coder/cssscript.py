from tkinter import *
from tkinter import ttk
from functools import partial
from lib import html as HTML
from lib import css as CSS

# Functions
# Respond to Category Button
def publish(text):
    list1.delete(0, END)
    thisList = []
    for x in CSS.cssProperties:
        if text in CSS.cssProperties[x]["type"]:
            thisList.append(x)
    for prop in thisList:
        list1.insert(END, prop)


def element(text):
    list1.delete(0, END)
    thisList = []
    for x in HTML.htmlTags:
        if text in HTML.htmlTags[x]["type"]:
            thisList.append(x)
    for elem in thisList:
        list1.insert(END, elem)


# Respond to Property Selection
def get_prop(event):
    global prop
    global description
    global value
    try:
        selected_tag = list1.curselection()[0]
        prop = list1.get(selected_tag)
        try:
            thisProp = dict(CSS.cssProperties.get(prop))
            value = thisProp.get("value")
        except:
            thisProp = dict(HTML.htmlTags.get(prop))
            value = "element:"
        description = thisProp.get("description")
    except IndexError:
        pass


def add_code():
    global v
    try:
        if "text:" in value:
            response = prop + ": " + value + ","
            list2.insert(END, response)
        elif "element:" in value:
            list2.insert(END, prop)
        else:
            v = Toplevel(root)
            i = 2
            ttk.Label(v, text="Select Value for " + prop + ":").grid(
                column=0, row=0, sticky=(N, W, E, S)
            )
            ttk.Label(v, text=description).grid(column=0, row=1, sticky=(N, W, E, S))
            for each in value:
                response = prop + ": " + each + ","
                ttk.Button(v, text=each, command=partial(select_value, response)).grid(
                    row=i, column=0, sticky=(N, W, E, S)
                )
                i = i + 1
            v.mainloop()
    except NameError:
        pass


def select_value(response):
    list2.insert(END, response)
    v.destroy()


def publish_pseudo(response):
    list2.insert(END, response)


# Open Curly Brackets
def start_element():
    list2.insert(END, "{")
    list2.insert(END, "")
    list2.insert(END, "}")


# Print to SASS
def print_sass():
    with open("output.scss", "w") as file:
        content = file.write("/ Output from Coder" + "\n")
        for each in list2.get(0, END):
            content = file.write(each + "\n")


# Close Program
def close_program():
    root.destroy()


# Define the Root
root = Tk()
root.title("CSS Coder")

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

# Top Line Buttons
ttk.Button(mainframe, text="code", command=add_code).grid(
    row=0, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="open", command=start_element).grid(
    row=0, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="Print", command=print_sass).grid(
    row=0, column=998, sticky=(W, E)
)

ttk.Button(mainframe, text="Close", command=close_program).grid(
    row=0, column=999, sticky=(W, E)
)

# HTML Buttons
ttk.Label(mainframe, text="Elements:").grid(row=1, column=0, sticky=(W, E))

ttk.Button(mainframe, text="Meta", command=partial(element, "meta")).grid(
    row=2, column=0, sticky=(W, E)
)
ttk.Button(mainframe, text="Event", command=partial(element, "event")).grid(
    row=3, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Heading", command=partial(element, "heading")).grid(
    row=4, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Text", command=partial(element, "text")).grid(
    row=5, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Media", command=partial(element, "media")).grid(
    row=6, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Containers", command=partial(element, "container")).grid(
    row=7, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Layout", command=partial(element, "layout")).grid(
    row=8, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Table", command=partial(element, "table")).grid(
    row=9, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Form", command=partial(element, "form")).grid(
    row=10, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="List", command=partial(element, "list")).grid(
    row=11, column=0, sticky=(W, E)
)

# Property Buttons
ttk.Label(mainframe, text="Properties:").grid(row=1, column=1, sticky=(W, E))

ttk.Button(mainframe, text="flex", command=partial(publish, "flex")).grid(
    row=2, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="meta", command=partial(publish, "meta")).grid(
    row=3, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="animation", command=partial(publish, "animation")).grid(
    row=4, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="responsive", command=partial(publish, "responsive")).grid(
    row=5, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="background", command=partial(publish, "background")).grid(
    row=6, column=1, sticky=(W, E)
)

ttk.Button(
    mainframe, text="borders", command=partial(publish, "borders and shading")
).grid(row=7, column=1, sticky=(W, E))

ttk.Button(mainframe, text="table", command=partial(publish, "table")).grid(
    row=8, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="position", command=partial(publish, "position")).grid(
    row=9, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="print", command=partial(publish, "print")).grid(
    row=10, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="layout", command=partial(publish, "layout")).grid(
    row=11, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="forms", command=partial(publish, "forms")).grid(
    row=12, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="font", command=partial(publish, "font")).grid(
    row=13, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="columns", command=partial(publish, "columns")).grid(
    row=14, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="paragraph", command=partial(publish, "paragraph")).grid(
    row=15, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="grid", command=partial(publish, "grid")).grid(
    row=16, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="list", command=partial(publish, "list")).grid(
    row=17, column=1, sticky=(W, E)
)

ttk.Button(mainframe, text="alignment", command=partial(publish, "alignment")).grid(
    row=18, column=1, sticky=(W, E)
)

# PseudoClass Buttons
p = 2
ttk.Label(mainframe, text="PseudoClass:").grid(row=1, column=998, sticky=(W, E))
for each in CSS.cssPseudoClass:
    ttk.Button(mainframe, text=each, command=partial(publish_pseudo, each)).grid(
        row=p, column=998, sticky=(W, E)
    )
    p = p + 1

# PseudoElement Buttons
p = 2
ttk.Label(mainframe, text="PseudoElement:").grid(row=1, column=999, sticky=(W, E))
for each in CSS.cssPseudoElement:
    ttk.Button(mainframe, text=each, command=partial(publish_pseudo, each)).grid(
        row=p, column=999, sticky=(W, E)
    )
    p = p + 1

# Selection Listbox
list1 = Listbox(mainframe, height=30, width=24)
list1.grid(row=1, column=2, rowspan=20, columnspan=2)
list1.bind("<<ListboxSelect>>", get_prop)

# Code Listbox
list2 = Listbox(mainframe, height=30, width=45)
list2.grid(row=1, column=4, rowspan=20, columnspan=4)

# Program Loop
root.mainloop()

