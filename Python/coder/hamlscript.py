from tkinter import *
from tkinter import ttk
from functools import partial
from lib import html as HTML
from lib import attr as ATTR

# Button Fx - Produce Lists
def publish(text):
    list1.delete(0, END)
    thisList = []
    for x in HTML.htmlTags:
        if text in HTML.htmlTags[x]["type"]:
            thisList.append(x)
    for tag in thisList:
        list1.insert(END, tag)


# Functions
def get_haml(event):
    try:
        global haml
        global attributes
        global tag
        global desc
        selected_tag = list1.curselection()[0]
        tag = list1.get(selected_tag)
        thisTag = dict(HTML.htmlTags.get(tag))
        haml = thisTag.get("haml")
        attributes = thisTag.get("attributes")
        desc = thisTag.get("description")
    except IndexError:
        pass


def select_code(event):
    global selected_code
    global selected_code_cursor
    selected_code_cursor = list2.curselection()[0]
    selected_code = list2.get(selected_code_cursor)


def edit_code():
    global e
    global t3
    # New Window
    e = Toplevel(root)
    e.title("Edit Line")

    t3 = Text(e, width=40, height=10, wrap="word")
    t3.grid(row=0, column=0)
    t3.insert("end", selected_code)

    ttk.Button(e, text="Abort", command=kill_edit_window).grid(
        row=998, column=0, sticky=(W, E)
    )

    ttk.Button(e, text="Accept", command=new_edit_code).grid(
        row=999, column=0, sticky=(W, E)
    )

    e.mainloop()


def kill_edit_window():
    e.destroy()


def new_edit_code():
    newText = t3.get("1.0", "end")
    s = list2.get(selected_code_cursor + 1, END)
    list2.delete(selected_code_cursor, END)
    list2.insert(selected_code_cursor, newText)
    for each in s:
        list2.insert(END, each)
    e.destroy()


def make_child():
    s = list2.get(selected_code_cursor + 1, END)
    list2.delete(selected_code_cursor, END)
    tab = "\t" + selected_code
    list2.insert(selected_code_cursor, tab)
    for each in s:
        list2.insert(END, each)


def make_parent():
    s = list2.get(selected_code_cursor + 1, END)
    list2.delete(selected_code_cursor, END)
    parent_code = selected_code.replace("\t", "")
    list2.insert(selected_code_cursor, parent_code)
    for each in s:
        list2.insert(END, each)


def publish_haml():
    try:
        list2.insert(END, code)
    except NameError:
        pass


def add_this():
    try:
        list2.insert(END, haml)
    except NameError:
        pass


def insert_here():
    try:
        list2.insert(ACTIVE, haml)
    except NameError:
        pass


def delete_code():
    list2.delete(selected_code_cursor)


def print_haml():
    global f
    global pathname
    f = Toplevel(root)
    f.title("Print New HAML File")

    ttk.Label(f, text="Enter the output filename:").grid(
        row=0, column=0, sticky=(N, W, S, E)
    )
    ttk.Label(f, text="example: output.haml").grid(row=1, column=0, sticky=(N, W, S, E))
    pathname = StringVar()
    e = ttk.Entry(f, textvariable=pathname).grid(row=2, column=0, sticky=(N, W, S, E))
    ttk.Button(f, text="Print", command=partial(print_new_haml, pathname)).grid(
        row=3, column=0, sticky=(N, W, S, E)
    )

    f.mainloop()


def print_new_haml(filename):
    path = filename.get()
    with open(path, "w") as file:
        content = file.write("/ Output from Coder:" + "\n")
        for each in list2.get(0, END):
            content = file.write(each + "\n")
    f.destroy()


def close_program():
    root.destroy()


# New Coding Window
def code_this():
    global c
    global attr_frame
    global t1
    # Create a new window
    c = Toplevel(root)
    c.title(tag)
    # Create New Frame
    cframe = ttk.Frame(c)
    cframe["padding"] = (5, 10)
    cframe["borderwidth"] = 2
    cframe["relief"] = "sunken"
    cframe.grid(column=0, row=0, sticky=(N, W, E, S))
    c.columnconfigure(0, weight=1)
    c.rowconfigure(0, weight=1)
    # Publish the element description
    ttk.Label(cframe, text=tag + " - " + desc).grid(
        row=0, column=0, columnspan=6, rowspan=3, sticky=(N, S, E, W)
    )
    # Create the coding box
    ttk.Label(cframe, text="Coding:").grid(row=4, column=0, columnspan=6, sticky=(E, W))
    t1 = Text(cframe, width=70, height=20, wrap="word")
    t1.grid(row=5, column=0)
    t1.insert(END, haml)
    # Create the Close Button, publishes text
    ttk.Button(cframe, text="Accept", command=close_c).grid(
        row=25, column=0, sticky=(E, W)
    )
    # Create Insert Button, inserts text
    ttk.Button(cframe, text="Insert", command=insert_c).grid(
        row=26, column=0, sticky=(E, W)
    )
    # Create the Abort Button
    ttk.Button(cframe, text="Abort", command=abort_c).grid(
        row=27, column=0, sticky=(E, W)
    )
    # Call method to create attribute buttons
    attr_frame = ttk.Frame(c)
    attr_frame["padding"] = (5, 10)
    attr_frame["borderwidth"] = 2
    attr_frame["relief"] = "sunken"
    attr_frame.grid(column=999, row=0, sticky=(N, W, E, S))
    list_attributes()

    t1.focus()
    c.mainloop()


# Close Attribute Window
def close_c():
    # Publish Contents in Haml Box
    z = t1.get("1.0", "end")
    list2.insert(END, z)
    # Close Window
    c.destroy()


def insert_c():
    # Publish Contents in Haml Box
    z = t1.get("1.0", "end")
    list2.insert(ACTIVE, z)
    # Close the Window
    c.destroy()


# Abort Attribute Window
def abort_c():
    c.destroy()


# Create attributes buttons
def list_attributes():
    ttk.Label(attr_frame, text="Local Attributes:").grid(
        column=0, row=0, sticky=(N, W, E, S)
    )
    i = 1
    # get local attributes
    for x in attributes:
        ttk.Button(attr_frame, text=x, command=partial(add_attribute, x)).grid(
            row=i, column=0, sticky=(N, W, E, S)
        )
        i = i + 1
    # get global attributes
    i = 0
    ttk.Label(attr_frame, text="Global Attributes:").grid(
        column=1, row=i, sticky=(N, W, E, S)
    )
    i = i + 1
    for x in ATTR.globalAttr:
        ttk.Button(attr_frame, text=x, command=partial(add_global, x)).grid(
            row=i, column=1, sticky=(N, W, E, S)
        )
        i = i + 1


def add_attribute(textAttr):
    if "text:" in ATTR.tagAttr[textAttr]["value"]:
        response = ATTR.tagAttr[textAttr]["value"]
        try:
            text = ", :" + textAttr + " => '" + response + "'"
            t1.insert("thismark", text)
        except:
            text = "{ :" + textAttr + " => '" + response + "' }"
            t1.insert(END, text)
            t1.mark_set("thismark", "end - 3c")
    elif type(ATTR.tagAttr[textAttr]["value"]) is str:
        response = ATTR.tagAttr[textAttr]["value"]
        try:
            text = ", :" + textAttr + " => '" + response + "'"
            t1.insert("thismark", text)
        except:
            text = "{ :" + textAttr + " => '" + response + "' }"
            t1.insert(END, text)
            t1.mark_set("thismark", "end - 3c")
    else:
        try:
            text = ", :" + textAttr + " => "
            t1.insert("thismark", text)
            open_values_window(textAttr)
        except:
            text = "{ :" + textAttr + " => "
            t1.insert(END, text)
            open_values_window(textAttr)


def open_values_window(textAttr):
    global v
    v = Toplevel(c)
    i = 1
    ttk.Label(v, text="Select Attribute Value For " + textAttr + ": ").grid(
        column=0, row=0, sticky=(N, W, E, S)
    )
    for each in ATTR.tagAttr[textAttr]["value"]:
        ttk.Button(v, text=each, command=partial(add_value, each)).grid(
            row=i, column=0, sticky=(N, W, E, S)
        )
        i = i + 1
    # Create the Close Button
    ttk.Button(v, text="Abort", command=abort_v).grid(
        row=999, column=0, sticky=(N, W, E, S)
    )
    v.mainloop()


# Add Value to t1
def add_value(x):
    try:
        text = "'" + x + "'"
        t1.insert("thismark", text)
    except:
        text = "'" + x + "' }"
        t1.insert(END, text)
        t1.mark_set("thismark", "end - 3c")
    v.destroy()


# Close Attribute Window
def abort_v():
    v.destroy()


def add_global(textAttr):
    try:
        text = ", :" + textAttr + " => 'INSERT TEXT'"
        t1.insert("thismark", text)
    except:
        text = "{ :" + textAttr + " => 'INSERT TEXT' }"
        t1.insert(END, text)
        t1.mark_set("thismark", "end - 3c")


# Define the Root
root = Tk()
root.title("Haml Coder")

# Define the Main Program Window
mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

# Layout the mainframe
ttk.Button(mainframe, text="Meta", command=partial(publish, "meta")).grid(
    row=1, column=0, sticky=(W, E)
)
ttk.Button(mainframe, text="Event", command=partial(publish, "event")).grid(
    row=2, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Heading", command=partial(publish, "heading")).grid(
    row=3, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Text", command=partial(publish, "text")).grid(
    row=4, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Media", command=partial(publish, "media")).grid(
    row=5, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Containers", command=partial(publish, "container")).grid(
    row=6, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Layout", command=partial(publish, "layout")).grid(
    row=7, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Table", command=partial(publish, "table")).grid(
    row=8, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="Form", command=partial(publish, "form")).grid(
    row=9, column=0, sticky=(W, E)
)

ttk.Button(mainframe, text="List", command=partial(publish, "list")).grid(
    row=10, column=0, sticky=(W, E)
)

# Topline Buttons
# Add
ttk.Button(mainframe, text="Add", command=add_this).grid(row=0, column=0, sticky=(W, E))

# Code
ttk.Button(mainframe, text="Code", command=code_this).grid(
    row=0, column=1, sticky=(W, E)
)

# Insert Code
ttk.Button(mainframe, text="Insert", command=insert_here).grid(
    row=0, column=2, sticky=(W, E)
)

# Edit Code
ttk.Button(mainframe, text="Edit", command=edit_code).grid(
    row=0, column=3, sticky=(W, E)
)

# Back Indent
ttk.Button(mainframe, text="Parent", command=make_parent).grid(
    row=0, column=4, sticky=(W, E)
)

# Indent
ttk.Button(mainframe, text="Child", command=make_child).grid(
    row=0, column=5, sticky=(W, E)
)

# Delete Line
ttk.Button(mainframe, text="Delete", command=delete_code).grid(
    row=0, column=6, sticky=(W, E)
)

# Print
ttk.Button(mainframe, text="Print", command=print_haml).grid(
    row=0, column=7, sticky=(W, E)
)

# Kill Program
ttk.Button(mainframe, text="Close", command=close_program).grid(
    row=0, column=8, sticky=(W, E)
)

# ListBoxes
list1 = Listbox(mainframe, height=12, width=45)
list1.grid(row=1, column=1, rowspan=10, columnspan=4)
list1.bind("<<ListboxSelect>>", get_haml)

list2 = Listbox(mainframe, height=12, width=45)
list2.grid(row=1, column=5, rowspan=10, columnspan=4)
list2.bind("<<ListboxSelect>>", select_code)

# Program Loop
root.mainloop()
