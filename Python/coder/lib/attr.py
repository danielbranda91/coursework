tagAttr = {
    "abbr": {
        "description": "species an abbreviated version of the content",
        "value": "text: abbreviation",
    },
    "accept": {
        "description": "filter for what file types the user can pick",
        "value": "text: file extensions",
    },
    "accept-charset": {
        "description": "the character encodings that are to be used for submission",
        "value": "text: character set",
    },
    "action": {"description": "where to send form data", "value": "text: URL"},
    "alt": {"description": "alternate text for the area", "value": "text"},
    "async": {
        "description": "the (external) script is executed asynchronously",
        "value": "async",
    },
    "autocomplete": {
        "description": "whether a form should have autocomplete on or off",
        "value": ["on", "off"],
    },
    "autofocus": {
        "description": "the element should automatically get focus when the page loads",
        "value": "autofocus",
    },
    "autoplay": {
        "description": "the audio will start playing when ready",
        "value": "autoplay",
    },
    "charset": {
        "description": "the character encoding used in an external script file",
        "value": "text: charset",
    },
    "checked": {
        "description": "input element should be pre-selected (radio/checkbox)",
        "value": "checked",
    },
    "cite": {"description": "the source of the content", "value": "text: URL"},
    "cols": {
        "description": "the visibile width of a text area",
        "value": "text: number",
    },
    "colspan": {"description": "the span of columns", "value": "text: number"},
    "content": {
        "description": "value associated with the http-equiv or name attribute. For example, a list of keywords, or the viewport setting",
        "value": "text: content",
    },
    "controls": {
        "description": "the audio controls should be displayed",
        "value": "controls",
    },
    "coords": {
        "description": "specific coordinates",
        "value": "text: x1,y1,x2,y2,xn,yn OR x,y,radius",
    },
    "crossorigin": {
        "description": "allow images from 3rd party sites that allow cross-origin access to be used",
        "value": ["anonymous", "use-credentials"],
    },
    "data": {"description": "the URL of the resource to be used", "value": "text: URL"},
    "datetime": {
        "description": "the date and time the element was changed",
        "value": "text: YYYY-MM-DDThh:mm:ss:TZD",
    },
    "defer": {
        "description": "execute the (external) script after the page has finished parsing",
        "value": "defer",
    },
    "disabled": {"description": "the element is disabled", "value": "disabled"},
    "download": {
        "description": "the target will be downloaded when a user clicks the link",
        "value": "text: filename",
    },
    "enctype": {
        "description": "how form data should be encoded upon submit",
        "value": [
            "application/x-www-form-urlencoded",
            "multipart/form-data",
            "text/plain",
        ],
    },
    "for": {
        "description": "used when an element is paired with another element, such as a label to an input",
        "value": "text: paired element id",
    },
    "form": {
        "description": "specifies one or more forms the element belongs to",
        "value": "text: form_id(s)",
    },
    "formaction": {
        "description": "where to send form-data on submit",
        "value": "text: URL",
    },
    "formenctype": {
        "description": "how form data should be encoded upon submit",
        "value": [
            "application/x-www-form-urlencoded",
            "multipart/form-data",
            "text/plain",
        ],
    },
    "formmethod": {
        "description": "how to send the form-data upon submit",
        "value": ["get", "post"],
    },
    "formnovalidate": {
        "description": "the form-data should not be validated on submit",
        "value": "formnovalidate",
    },
    "formtarget": {
        "description": "where to display the response after submit",
        "value": ["_blank", "_self", "_parent", "_top"],
    },
    "headers": {
        "description": "one or more header cells this cell is related to",
        "value": "text: header id",
    },
    "height": {"description": "element height", "value": "text: element height"},
    "high": {"description": "the high value of the range", "value": "text: number"},
    "href": {"description": "the url that the link goes to", "value": "text: URL"},
    "http-equiv": {
        "description": "an http header for the information/value of content attribute",
        "value": ["content-type", "default-style", "refresh"],
    },
    "ismap": {"description": "an image is a server-side image-map", "value": "ismap"},
    "kind": {
        "description": "the kind of text track",
        "value": ["captions", "chapters", "descriptions", "metadata", "subtitles"],
    },
    "label": {"description": "text label for an option-group", "value": "text: label"},
    "list": {
        "description": "refers to a datalist element with pre-defined options",
        "value": "text: datalist id",
    },
    "longdesc": {
        "description": "a URL with a detailed description of an image",
        "value": "text: URL",
    },
    "loop": {
        "description": "media will start over again when finished",
        "value": "loop",
    },
    "low": {"description": "the low value of the range", "value": "text: number"},
    "max": {
        "description": "the maximum value for an element",
        "value": "text: either number or date",
    },
    "maxlength": {
        "description": "the maximum number of characters allowed for an element",
        "value": "text: number",
    },
    "media": {
        "description": "media/device that the linked document is optimized for",
        "value": [
            "all",
            "aural",
            "braille",
            "handheld",
            "projection",
            "print",
            "screen",
            "tty",
            "tv",
        ],
    },
    "method": {
        "description": "how to send the form-data upon submit",
        "value": ["get", "post"],
    },
    "min": {
        "description": "the minimum value for an element",
        "value": "text: either number or date",
    },
    "multiple": {
        "description": "a user can enter more than one value",
        "value": "multiple",
    },
    "muted": {"description": "media will be muted", "value": "muted"},
    "name": {
        "description": "name for the element",
        "value": "text: name. For meta, name = application-name|author|description|generator|keywords|viewport",
    },
    "novalidate": {
        "description": "the form-data should not be validated on submit",
        "value": "novalidate",
    },
    "open": {
        "description": "the element should be open/visible to user",
        "value": "open",
    },
    "optimum": {
        "description": "the optimal value for the gauge",
        "value": "text: number",
    },
    "pattern": {
        "description": "a regular expression that an element's value is checked against",
        "value": "text: regexp",
    },
    "ping": {
        "description": "space separated list of urls to which, when the link is followed, post requests with the body ping will be sent",
        "value": "text: List URLs",
    },
    "placeholder": {
        "description": "short hint that describes expected value",
        "value": "text: placeholder",
    },
    "poster": {
        "description": "an image to be shown when the video is not playing",
        "value": "text: image URL",
    },
    "preload": {
        "description": "if and how the media should be loaded with the page",
        "value": ["auto", "metadata", "none"],
    },
    "readonly": {"description": "input field is readonly", "value": "readonly"},
    "referrerpolicy": {
        "description": "specifies which referrer to send",
        "value": [
            "no-referrer",
            "no-referrer-when-downgrade",
            "origin",
            "origin-when-cross-origin",
            "unsafe-url",
        ],
    },
    "rel": {
        "description": "relationship between current and linked documents",
        "value": [
            "alternate",
            "author",
            "bookmark",
            "dns-prefetch",
            "external",
            "help",
            "icon",
            "license",
            "next",
            "nofollow",
            "noreferrer",
            "noopener",
            "pingback",
            "preload",
            "prerender",
            "prev",
            "search",
            "stylesheet",
            "tag",
        ],
    },
    "required": {
        "description": "the input field must be completed",
        "value": "required",
    },
    "reversed": {
        "description": "the list order should be descending",
        "value": "reversed",
    },
    "rows": {"description": "the visible number of lines", "value": "text: number"},
    "rowspan": {"description": "number of rows a cell spans", "value": "text: number"},
    "sandbox": {
        "description": "restrictions on iframe content",
        "values": [
            "allow-forms",
            "allow-pointer-lock",
            "allow-popups",
            "allow-same-origin",
            "allow-scripts",
            "allow-top-navigation",
        ],
    },
    "scope": {
        "description": "whether a header cell is a header for a column, row, or group",
        "value": ["col", "colgroup", "row", "rowgroup"],
    },
    "selected": {"description": "the item is selected by default", "value": "selected"},
    "shape": {
        "description": "the shape of the area",
        "value": ["default", "rect", "circle", "poly"],
    },
    "size": {
        "description": "the size, in characters, of an element",
        "value": "text: number",
    },
    "sizes": {
        "description": "image sizes for different page layouts",
        "value": "text: sizes or HeightxWidth",
    },
    "sorted": {
        "description": "the sort direction of a column",
        "value": ["reversed", "number", "reversed number", "number reversed"],
    },
    "span": {"description": "the number of columns to span", "value": "text: number"},
    "src": {"description": "the url of the source media", "value": "text: URL"},
    "srcdoc": {
        "description": "the html content of the page to show in the iframe",
        "value": "text: HTML code",
    },
    "srcset": {
        "description": "the URL of an image to use in different situations",
        "value": "text: URL",
    },
    "start": {"description": "the start value of the object", "value": "text: number"},
    "step": {
        "description": "the interval between legal numbers in an input field",
        "value": "text: number",
    },
    "target": {
        "description": "where to open the linked document",
        "value": ["_blank", "_parent", "_self", "_top"],
    },
    "type": {
        "description": "the element type",
        "value": "text: media type, or button: button|reset|submit, or input: button, checkbox, color, date, datetime-local, email, file, hidden, image, month, number, password, radio, range, reset, search, submit, tel, text, time, url, week, or OL: 1|A|a|I|i, or for style: text/css",
    },
    "usemap": {
        "description": "image is a client-side image-map",
        "value": "text: mapname",
    },
    "value": {
        "description": "initial value for the element",
        "value": "text: initial value",
    },
    "width": {"description": "element width", "value": "text: element width"},
    "wrap": {
        "description": "how the text in a container is to be wrapped",
        "value": ["hard", "soft"],
    },
    "xmlns": {
        "description": "the XML namespace attribute",
        "value": "http://www.w3.org/1999/xhtml",
    },
}

globalAttr = {
    "accesskey": {"description": "shortcut key to activate/focus on element"},
    "class": {"description": "classname(s) for the element"},
    "contenteditable": {"description": "whether the content is editable"},
    "data-": {"description": "store custom data private to the page/application"},
    "dir": {"description": "specifies the text direction"},
    "draggable": {"description": "specifies if the element is draggable"},
    "dropzone": {
        "description": "species whether the dragged data is copied, movie or linked"
    },
    "hidden": {"description": "an element is not yet, or no longer, relevant"},
    "id": {"description": "a unique id for the element"},
    "lang": {"description": "the language of the element's content"},
    "spellcheck": {
        "description": "whether the element is to have its spelling/grammar checked"
    },
    "style": {"description": "inline css"},
    "tabindex": {"description": "tabbing order of an element"},
    "title": {"description": "extra information about the element"},
    "translate": {"description": "whether the content should be translated"},
}

