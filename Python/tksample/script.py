# import tkinter
from tkinter import *
from tkinter import ttk

# define the calculate procedure, placing the result in the label widget
def calculate(*args):
    try:
        value = float(feet.get())
        meters.set((0.3048 * value * 10000.0 + 0.5) / 10000.0)
    except ValueError:
        pass


# Set up the main window, with a frame widget in our main window
# Also, configure columns and rows for resizing
root = Tk()
root.title("Feet to Meters")

mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

# Create the three main widgets (entry, label, button), with textvariables assigned to the entry and label widgets
# Sticky says how the element aligns within the cell. W, E means the element spans the full width of the cell.
feet = StringVar()
meters = StringVar()

feet_entry = ttk.Entry(mainframe, width=7, textvariable=feet)
feet_entry.grid(column=2, row=1, sticky=(W, E))

ttk.Label(mainframe, textvariable=meters).grid(column=2, row=2, sticky=(W, E))
ttk.Button(mainframe, text="Calculate", command=calculate).grid(
    column=3, row=3, sticky=W
)

ttk.Label(mainframe, text="feet").grid(column=3, row=1, sticky=W)
ttk.Label(mainframe, text="is equivalent to").grid(column=1, row=2, sticky=E)
ttk.Label(mainframe, text="meters").grid(column=3, row=2, sticky=W)

# pads each element, which is considered a child of the mainframe
for child in mainframe.winfo_children():
    child.grid_configure(padx=5, pady=5)

# cursor starts in the entry field
feet_entry.focus()
# If the user presses Enter, it will calculate--just as if the user pressed the calculate button
root.bind("<Return>", calculate)

# Execute the Loop
root.mainloop()
