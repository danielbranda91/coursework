# https://tkdocs.com/tutorial/onepage.html
# Note: I am a reference. I am not supposed to work.
# import tkinter
from tkinter import *
from tkinter import ttk

# Frame
frame = ttk.Frame(parent)
frame["padding"] = (5, 10)
frame["borderwidth"] = 2
frame["relief"] = "sunken"

# Label
label = ttk.Label(parent, text="Full name:")
# Displaying Text
resultsContents = StringVar()
label["textvariable"] = resultsContents
resultsContents.set("New value to display")

# Label Frames / Groupboxes
lf = ttk.Labelframe(parent, text='Label')
# Paned Windows
p = ttk.Panedwindow(parent, orient=VERTICAL)
# first pane, which would get widgets gridded into it:
f1 = ttk.Labelframe(p, text='Pane1', width=100, height=100)
f2 = ttk.Labelframe(p, text='Pane2', width=100, height=100)   # second pane
p.add(f1)
p.add(f2)

# Notebooks - a tabbed notebook to let the user switch between one of several indexed 'pages'
n = ttk.Notebook(parent)
f1 = ttk.Frame(n)   # first page, which would get widgets gridded into it
f2 = ttk.Frame(n)   # second page
n.add(f1, text='One')
n.add(f2, text='Two')

# Images
image = PhotoImage(file="myimage.gif")
label["image"] = image

# Buttons
# The button can take an image just as a Label takes an image, through a property
button = ttk.Button(parent, text="Okay", command=submitForm)

# Checkbutton. Value is 1 or 0, captured by measureSystem
measureSystem = StringVar()
check = ttk.Checkbutton(
    parent,
    text="Use Metric",
    command=metricChanged,
    variable=measureSystem,
    onvalue="metric",
    offvalue="imperial",
)

# Checkbuttons use many of the same options as regular buttons.
# To manipulate a button/checkbutton's state:
button.state(["disabled"])  # set the disabled flag, disabling the button
button.state(["!disabled"])  # clear the disabled flag
button.instate(["disabled"])  # return true if the button is disabled, else false
button.instate(["!disabled"])  # return true if the button is not disabled, else false
button.instate(["!disabled"], cmd)  # execute 'cmd' if the button is not disabled

# Radiobutton - to choose through multiple choices, captured by phone variable
phone = StringVar()
home = ttk.Radiobutton(parent, text="Home", variable=phone, value="home")
office = ttk.Radiobutton(parent, text="Office", variable=phone, value="office")
cell = ttk.Radiobutton(parent, text="Mobile", variable=phone, value="cell")

# Entry
username = StringVar()
name = ttk.Entry(parent, textvariable=username)

# Get or Change the value with delete and insert
print("current value is %s" % name.get())
name.delete(0, "end")  # delete between two indices, 0-based
name.insert(0, "your name")  # insert new text at a given index

# To Hide Text Input, like Passwords
name["show"] = "*"

# Combo Box - set and get methods can apply
countryvar = StringVar()
country = ttk.Combobox(parent, textvariable=countryvar)
# Combo Box generates a selection event that can bind when its value changes
country.bind("<<ComboboxSelected>>", function)
# Set the list of values
country["values"] = ("USA", "Canada", "Australia")
# To determine which value of the list is selected
country.current()  # returns the index of the value

# Listboxes
l = Listbox(parent, height=10)
## See listbox.py for usage

# Scroll Bar, where parent is defined as the widget to scroll
s = ttk.Scrollbar(parent, orient=VERTICAL, command=listbox.yview)
listbox.configure(yscrollcommand=s.set)

# Size Grip
ttk.Sizegrip(parent).grid(column=999, row=999, sticky=(S, E))

# Text Area
t = Text(parent, width=40, height=10, wrap="word")
t["state"] = "disabled"
t["state"] = "normal"
# to get, "get 1.0 end" to retrieve from line 1, character 0, through end
# to insert, "insert 3.5 'string entered here'" to insert the string at line 3, character 5

# Progress Bar
p = ttk.Progressbar(parent, orient=HORIZONTAL, length=200, mode="determinate")

# Scale / Slide Rule
s = ttk.Scale(parent, orient=HORIZONTAL, length=200, from_=1.0, to=100.0)

# Spinbox
spinval = StringVar()
s = Spinbox(parent, from_=1.0, to=100.0, textvariable=spinval)

# Separators/ HRs
s = ttk.Separator(parent, orient=HORIZONTAL)

# Menus - Setup
root.tk.call("tk", "windowingsystem")  # will return x11, win32 or aqua
root.option_add("*tearOff", FALSE)
# Adding Menu Bar with Menu Widget
win = Toplevel(root)
menubar = Menu(win)
win["menu"] = menubar
# Adding Menus with a Menu Widget as a Child to the Menu Bar
menubar = Menu(parent)
menu_file = Menu(menubar)
menu_edit = Menu(menubar)
menubar.add_cascade(menu=menu_file, label="File")
menubar.add_cascade(menu=menu_edit, label="Edit")
# Adding Items to Each Menu
menu_file.add_command(label="New", command=newFile)
menu_file.add_command(label="Open...", command=openFile)
menu_file.add_command(label="Close", command=closeFile)
# Add a Separator in a Menu
menu_file.add_separator()
# Add a Checkbutton or Radiobutton menu item
check = StringVar()
menu_file.add_checkbutton(label="Check", variable=check, onvalue=1, offvalue=0)
radio = StringVar()
menu_file.add_radiobutton(label="One", variable=radio, value=1)
menu_file.add_radiobutton(label="Two", variable=radio, value=2)
# Add Hotkeys to Menu Items, use the "accelerator" value and assign a string to state the hotkey. Then, bind the hotkeys to the menu item.
# To create a help menu
helpmenu = Menu(menubar, name="help")
menubar.add_cascade(menu=helpmenu, label="Help")
root.createcommand("tk::mac::ShowHelp", ...)
# Similar Menu Handlers
# tk::mac::ShowPreferences
# tk::mac::Quit
# tk::mac::OnHide
# tk::mac::OnShow
# tk::mac::OpenApplication
# tk::mac::ReopenApplication
# tk::mac::OpenDocument
# tk::mac::PrintDocument 

# To create a window menu
windowmenu = Menu(menubar, name="window")
menubar.add_cascade(menu=windowmenu, label="Window")

# Contextual Menus (Right-Click)
from tkinter import *
root = Tk()
menu = Menu(root)
for i in ('One', 'Two', 'Three'):
    menu.add_command(label=i)
if (root.tk.call('tk', 'windowingsystem')=='aqua'):
    root.bind('<2>', lambda e: menu.post(e.x_root, e.y_root))
    root.bind('<Control-1>', lambda e: menu.post(e.x_root, e.y_root))
else:
    root.bind('<3>', lambda e: menu.post(e.x_root, e.y_root))
root.mainloop()

# Multiple Windows
# Create a new toplevel window. In this example, the parent is the root window
t = Toplevel(parent)
# To destroy the window
t.destroy() 
# Note that the destroy method works on any widget

#Dialog Boxes
# To let the user find a file
from tkinter import filedialog
filename = filedialog.askopenfilename()
filename = filedialog.asksaveasfilename()
dirname = filedialog.askdirectory()

# To let the user pick a color
from tkinter import colorchooser
colorchooser.askcolor(initialcolor='#ff0000')

# Alert/Confirmation
from tkinter import messagebox
messagebox.showinfo(message='Have a good day')
messagebox.askyesno(
	   message='Are you sure you want to install SuperVirus?'
	   icon='question' title='Install')
# Other Options
# askokcancel()
# askquestion()
# askretrycancel()
# askyesno()
# askyesnocancel()
# showerror()
# showinfo()
# showwarning()

# Fonts
from tkinter import font
appHighlightFont = font.Font(family='Helvetica', size=12, weight='bold')
ttk.Label(root, text='Attention!', font=appHighlightFont).grid()
# To Get the Names of Available Fonts:
# font.families()

# Tree Objects
tree = ttk.Treeview(parent)
# Inserted at the root, program chooses id:
tree.insert('', 'end', 'widgets', text='Widget Tour')
# Same thing, but inserted as first child:
tree.insert('', 0, 'gallery', text='Applications')
# Treeview chooses the id:
id = tree.insert('', 'end', text='Tutorial')
# Inserted underneath an existing node:
tree.insert('widgets', 'end', text='Canvas')
tree.insert(id, 'end', text='Tree')
# Moving nodes in tree
tree.move('widgets', 'gallery', 'end')  # move widgets under gallery
# Detaching items from the tree
tree.detach('widgets')
# Deleting items from the tree
tree.delete('widgets')
