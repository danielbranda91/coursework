# requires web-based installation of postgreSQL psycopg2
# requires database_credentials.txt file:
# user: username
# password: dbpassword
# port: 1234

import pyscopg2

# create the database through postgreSQL first
def create_table():
    conn = psycopg2.connect(
        "dbname='database1' user='username' password='dbpassword' host='localhost' port='1234'"
    )
    cur = conn.cursor()
    cur.execute(
        "CREATE TABLE IF NOT EXISTS store (item TEXT, quantity INTEGER, price REAL)"
    )

    conn.commit()
    conn.close()


create_table()


def insert(item, quantity, price):
    conn = conn = psycopg2.connect(
        "dbname='database1' user='username' password='dbpassword' host='localhost' port='1234'"
    )
    # Creates the db if it does not exist
    cur = conn.cursor()
    cur.execute("INSERT INTO store VALUES (%s,%s,%s)", (item, quantity, price))
    conn.commit()
    conn.close()


insert("Wine Glass", 8, 10.5)
insert("Water Glass", 10, 5.00)
insert("Coffee Cup", 12, 4.8)


def delete(item):
    conn = conn = psycopg2.connect(
        "dbname='database1' user='username' password='dbpassword' host='localhost' port='1234'"
    )
    cur = conn.cursor()
    cur.execute("DELETE FROM store WHERE item=%s", (item,))
    conn.commit()
    conn.close()


delete("Wine Glass")


def update(quantity, price, item):
    conn = conn = psycopg2.connect(
        "dbname='database1' user='username' password='dbpassword' host='localhost' port='1234'"
    )
    cur = conn.cursor()
    cur.execute(
        "UPDATE store SET quantity=%s, price=%s WHERE item=%s", (quantity, price, item)
    )
    conn.commit()
    conn.close()


update(13, 5, "Coffee Cup")


def view():
    conn = conn = psycopg2.connect(
        "dbname='database1' user='username' password='dbpassword' host='localhost' port='1234'"
    )
    cur = conn.cursor()
    cur.execute("SELECT * FROM store")
    rows = cur.fetchall()
    conn.close()
    return rows


print(view())

