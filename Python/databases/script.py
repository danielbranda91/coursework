import sqlite3


def create_table():
    conn = sqlite3.connect("lite.db")
    # Creates the db if it does not exist
    cur = conn.cursor()
    cur.execute(
        "CREATE TABLE IF NOT EXISTS store (item TEXT, quantity INTEGER, price REAL)"
    )

    conn.commit()
    conn.close()


create_table()


def insert(item, quantity, price):
    conn = sqlite3.connect("lite.db")
    # Creates the db if it does not exist
    cur = conn.cursor()
    cur.execute("INSERT INTO store VALUES (?,?,?)", (item, quantity, price))
    conn.commit()
    conn.close()


insert("Wine Glass", 8, 10.5)
insert("Water Glass", 10, 5.00)
insert("Coffee Cup", 12, 4.8)


def delete(item):
    conn = sqlite3.connect("lite.db")
    cur = conn.cursor()
    cur.execute("DELETE FROM store WHERE item=?", (item,))
    conn.commit()
    conn.close()


delete("Wine Glass")


def update(quantity, price, item):
    conn = sqlite3.connect("lite.db")
    cur = conn.cursor()
    cur.execute(
        "UPDATE store SET quantity=?, price=? WHERE item=?", (quantity, price, item)
    )
    conn.commit()
    conn.close()


update(13, 5, "Coffee Cup")


def view():
    conn = sqlite3.connect("lite.db")
    cur = conn.cursor()
    cur.execute("SELECT * FROM store")
    rows = cur.fetchall()
    conn.close()
    return rows


print(view())

