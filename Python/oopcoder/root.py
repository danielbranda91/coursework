# Next To Do: Convert CSS Listbox2 into a Text Box, and alter selection commands
# Next To Do: Add Edit, Delete, et al buttons to CSS

from tkinter import *
from tkinter import ttk
from functools import partial
from lib import html
from lib import css
from lib import attr


class Root:
    def __init__(self):
        self.root = Tk()
        self.root.wm_title("Coder")
        self.root.minsize(width=200, height=200)

        ttk.Button(
            self.root, text="HTML", command=partial(self.load_coder, "html")
        ).grid(row=1, column=1, sticky=(N, S, E, W))
        ttk.Button(self.root, text="CSS", command=partial(self.load_coder, "css")).grid(
            row=2, column=1, sticky=(N, S, E, W)
        )
        ttk.Button(self.root, text="Exit", command=self.exit_program).grid(
            row=3, column=1, sticky=(N, S, E, W)
        )

        self.root.columnconfigure(0, weight=1)
        self.root.rowconfigure(0, weight=1)
        self.root.columnconfigure(1, weight=3)
        self.root.rowconfigure(1, weight=3)
        self.root.rowconfigure(2, weight=3)
        self.root.columnconfigure(2, weight=1)
        self.root.rowconfigure(3, weight=3)
        self.root.rowconfigure(4, weight=1)

        self.root.mainloop()

    def load_coder(self, x):
        # mainframe = Mainframe(self.root, x)
        if x is "html":
            # mainframe = HamlScript(self.root)
            mainframe = HamlScript(self.root)
        elif x is "css":
            mainframe = CssScript(self.root)
        else:
            print("Something unexpected happened")

    def exit_program(self):
        self.root.destroy()


class Mainframe:
    def __init__(self, root):
        self.root = root
        self.m = Toplevel(self.root, padx=12, pady=3)
        self.m.minsize(height=200, width=200)
        # Topline Buttons
        # Select from List and Code
        ttk.Button(self.m, text="Code", command=self.code_this).grid(
            row=0, column=0, sticky=(W, E)
        )
        # Delete the Selected Code
        ttk.Button(self.m, text="Delete", command=self.delete_code).grid(
            row=0, column=6, sticky=(W, E)
        )
        # Print to an Output File
        ttk.Button(self.m, text="Print", command=self.print_code).grid(
            row=0, column=7, sticky=(W, E)
        )
        # Kill the Main Window
        ttk.Button(self.m, text="Close", command=self.close_frame).grid(
            row=0, column=8, sticky=(W, E)
        )

    # Publish Type Lists as Buttons
    def typeButtons(self, types, q, col, r):
        i = r
        for each in types:
            ttk.Button(self.m, text=each, command=partial(self.publish, each, q)).grid(
                row=i, column=col, sticky=(W, E)
            )
            i = i + 1

    def publish(self, text, q):
        if q == "pseudo":
            self.list2.insert(END, text)
        else:
            try:
                self.list1.delete(0, END)
                thisList = []
                for x in q:
                    if text in q[x]["type"]:
                        thisList.append(x)
                for each in thisList:
                    self.list1.insert(END, each)
            except:
                pass

    def delete_code(self):
        try:
            self.list2.delete(self.selected_code_cursor)
        except:
            pass

    def print_code(self):
        try:
            self.f = Toplevel(self.root)
            self.f.title("Create New File")

            ttk.Label(self.f, text="Enter the output filename:").grid(
                row=0, column=0, sticky=(N, W, S, E)
            )
            self.pathname = StringVar()
            e = ttk.Entry(self.f, textvariable=self.pathname).grid(
                row=1, column=0, sticky=(N, W, S, E)
            )
            ttk.Button(
                self.f, text="Save", command=partial(self.print_new_file, self.pathname)
            ).grid(row=2, column=0, sticky=(N, W, S, E))
            self.f.mainloop()
        except:
            pass

    def close_frame(self):
        self.m.destroy()

    def select_code(self, event):
        try:
            self.selected_code_cursor = self.list2.curselection()[0]
            self.selected_code = self.list2.get(self.selected_code_cursor)
        except:
            pass

    def print_new_file(self, filename):
        path = "output/" + filename.get()
        with open(path, "w") as file:
            content = file.write("/ Output from Coder:" + "\n")
            for each in self.list2.get(0, END):
                content = file.write(each + "\n")
        self.f.destroy()


class HamlScript(Mainframe):
    def __init__(self, root):
        Mainframe.__init__(self, root)
        self.m.title("Haml Code Editor")

        self.codeTypes = html.codeTypes
        self.tags = html.htmlTags
        self.typeButtons(self.codeTypes, self.tags, 0, 1)

        # Topline Buttons
        # Add Directly from List without Coding
        ttk.Button(self.m, text="Add", command=self.add_this).grid(
            row=0, column=1, sticky=(W, E)
        )
        # Insert Directly Above Selected Line without Coding
        ttk.Button(self.m, text="Insert", command=self.insert_here).grid(
            row=0, column=2, sticky=(W, E)
        )
        # Edit the Selected Code
        ttk.Button(self.m, text="Edit", command=self.edit_code).grid(
            row=0, column=3, sticky=(W, E)
        )
        # Back Indent the Selected Code
        ttk.Button(self.m, text="Parent", command=self.make_parent).grid(
            row=0, column=4, sticky=(W, E)
        )
        # Indent the Selected Code
        ttk.Button(self.m, text="Child", command=self.make_child).grid(
            row=0, column=5, sticky=(W, E)
        )

        # Lisboxes
        # Selection Listbox
        self.list1 = Listbox(self.m, height=12, width=45)
        self.list1.grid(row=1, column=1, rowspan=10, columnspan=4)
        self.list1.bind("<<ListboxSelect>>", self.get_code)
        # Output Listbox
        self.list2 = Listbox(self.m, height=12, width=45)
        self.list2.grid(row=1, column=5, rowspan=10, columnspan=4)
        self.list2.bind("<<ListboxSelect>>", self.select_code)

        self.m.mainloop()

    # Functions for Topline Code
    def code_this(self):
        try:
            self.codewindow = CodeWindow(
                self.root, self.tag, self.desc, self.haml, self.list2, self.attributes
            )
        except:
            pass

    def get_code(self, event):
        try:
            selected_tag = self.list1.curselection()[0]
            self.tag = self.list1.get(selected_tag)
            thisTag = dict(html.htmlTags.get(self.tag))
            self.haml = thisTag.get("haml")
            self.attributes = thisTag.get("attributes")
            self.desc = thisTag.get("description")
        except IndexError:
            pass

    def add_this(self):
        try:
            self.list2.insert(END, self.haml)
        except NameError:
            pass

    def insert_here(self):
        try:
            self.list2.insert(ACTIVE, self.haml)
        except NameError:
            pass

    def edit_code(self):
        self.e = Toplevel(self.root)
        self.e.title = "Edit Line"
        self.t3 = Text(self.e, width=40, height=10, wrap="word")
        self.t3.grid(row=0, column=0)
        self.t3.insert("end", self.selected_code)

        ttk.Button(self.e, text="Abort", command=self.kill_edit_window).grid(
            row=998, column=0, sticky=(W, E)
        )

        ttk.Button(self.e, text="Accept", command=self.new_edit_code).grid(
            row=999, column=0, sticky=(W, E)
        )

        self.e.mainloop()

    def make_parent(self):
        s = self.list2.get(self.selected_code_cursor + 1, END)
        self.list2.delete(self.selected_code_cursor, END)
        parent_code = self.selected_code.replace("\t", "")
        self.list2.insert(self.selected_code_cursor, parent_code)
        for each in s:
            self.list2.insert(END, each)

    def make_child(self):
        s = self.list2.get(self.selected_code_cursor + 1, END)
        self.list2.delete(self.selected_code_cursor, END)
        tab = "\t" + self.selected_code
        self.list2.insert(self.selected_code_cursor, tab)
        for each in s:
            self.list2.insert(END, each)

    def kill_edit_window(self):
        self.e.destroy()

    def new_edit_code(self):
        newText = self.t3.get("1.0", "end")
        s = self.list2.get(self.selected_code_cursor + 1, END)
        self.list2.delete(self.selected_code_cursor, END)
        self.list2.insert(self.selected_code_cursor, newText)
        for each in s:
            self.list2.insert(END, each)
        self.e.destroy()


class CssScript(Mainframe):
    def __init__(self, root):
        Mainframe.__init__(self, root)
        self.m.title("CSS Code Editor")
        self.codeTypes = css.codeTypes
        self.cssTags = css.cssProperties
        self.htmlTypes = html.codeTypes
        self.htmlTags = html.htmlTags
        self.pseudoElements = css.cssPseudoElement
        self.pseudoClasses = css.cssPseudoClass
        ttk.Label(self.m, text="Elements:").grid(row=1, column=0, sticky=(W, E))
        ttk.Label(self.m, text="Properties:").grid(row=1, column=1, sticky=(W, E))
        self.typeButtons(self.codeTypes, self.cssTags, 1, 2)
        self.typeButtons(self.htmlTypes, self.htmlTags, 0, 2)

        # PseudoClass Buttons
        ttk.Label(self.m, text="PseudoClass:").grid(row=1, column=998, sticky=(W, E))
        self.typeButtons(self.pseudoClasses, "pseudo", 998, 2)

        # PseudoElement Buttons
        ttk.Label(self.m, text="PseudoElement:").grid(row=1, column=999, sticky=(W, E))
        self.typeButtons(self.pseudoElements, "pseudo", 999, 2)

        # Topline Buttons
        ttk.Button(self.m, text="open", command=self.start_element).grid(
            row=0, column=1, sticky=(W, E)
        )

        # Lisboxes
        # Selection Listbox
        self.list1 = Listbox(self.m, height=30, width=24)
        self.list1.grid(row=1, column=2, rowspan=20, columnspan=2)
        self.list1.bind("<<ListboxSelect>>", self.get_code)
        # Output Listbox
        self.list2 = Listbox(self.m, height=30, width=45)
        self.list2.grid(row=1, column=4, rowspan=20, columnspan=4)
        self.list2.bind("<<ListboxSelect>>", self.select_code)

        self.m.mainloop()

    def code_this(self):
        try:
            if "text:" in self.value:
                response = "\t" + self.tag + ": " + self.value + ", "
                self.list2.insert(END, response)
            elif "element:" in self.value:
                self.list2.insert(END, self.tag)
            else:
                self.v = Toplevel(self.root)
                i = 2
                ttk.Label(self.v, text="Select Value for " + self.tag + ":").grid(
                    column=0, row=0, sticky=(N, W, E, S)
                )
                ttk.Label(self.v, text=self.description).grid(
                    column=0, row=1, sticky=(N, W, E, S)
                )
                for each in self.value:
                    response = "\t" + self.tag + ": " + each + ","
                    ttk.Button(
                        self.v, text=each, command=partial(self.select_value, response)
                    ).grid(row=i, column=0, sticky=(N, W, E, S))
                    i = i + 1
                self.v.mainloop()
        except:
            pass

    def get_code(self, event):
        try:
            selected_tag = self.list1.curselection()[0]
            self.tag = self.list1.get(selected_tag)
            try:
                thisProp = dict(css.cssProperties.get(self.tag))
                self.value = thisProp.get("value")
            except:
                thisProp = dict(html.htmlTags.get(self.tag))
                self.value = "element:"
            self.description = thisProp.get("description")
        except IndexError:
            pass

    def select_value(self, response):
        self.list2.insert(END, response)
        self.v.destroy()

    def start_element(self):
        self.list2.insert(END, "{")
        self.list2.insert(END, "")
        self.list2.insert(END, "}")


class CodeWindow:
    def __init__(self, anchor, tag, desc, code, target, attributes):
        self.c = Toplevel(anchor)
        self.c.title(tag)
        self.wordbox = target
        self.attributes = attributes

        cframe = ttk.Frame(self.c)
        cframe["padding"] = (5, 10)
        cframe["borderwidth"] = 2
        cframe["relief"] = "sunken"
        cframe.grid(column=0, row=0, sticky=(N, W, E, S))
        self.c.columnconfigure(0, weight=1)
        self.c.rowconfigure(0, weight=1)

        ttk.Label(cframe, text=tag + " - " + desc).grid(
            row=0, column=0, columnspan=6, rowspan=3
        )

        # The Coding Box
        ttk.Label(cframe, text="Coding:").grid(
            row=4, column=0, columnspan=6, sticky=(E, W)
        )
        self.t1 = Text(cframe, width=70, height=20, wrap="word")
        self.t1.grid(row=5, column=0)
        self.t1.insert(END, code)

        # The Close/Publish Button
        ttk.Button(cframe, text="Accept", command=self.close_c).grid(
            row=25, column=0, sticky=(E, W)
        )
        # The Insert Button
        ttk.Button(cframe, text="Insert", command=self.insert_c).grid(
            row=26, column=0, sticky=(E, W)
        )
        # The Abort Button
        ttk.Button(cframe, text="Abort", command=self.abort_c).grid(
            row=27, column=0, sticky=(E, W)
        )
        # The Attributes Buttons
        self.attr_frame = CodeFrame(self.c)
        self.attr_frame["padding"] = (5, 10)
        self.attr_frame["borderwidth"] = 2
        self.attr_frame["relief"] = "sunken"
        self.attr_frame.grid(column=999, row=0, sticky=(N, W, E, S))
        self.list_attributes()

        self.t1.focus()
        self.c.mainloop()

    def close_c(self):
        # Publish Contents in Wordbox
        z = self.t1.get("1.0", "end")
        self.wordbox.insert(END, z)
        # Close
        self.c.destroy()

    def insert_c(self):
        z = self.t1.get("1.0", "end")
        self.wordbox.insert(ACTIVE, z)
        # Close the Window
        self.c.destroy()

    def abort_c(self):
        self.c.destroy()

    def list_attributes(self):
        ttk.Label(self.attr_frame, text="Local Attributes:").grid(
            column=0, row=0, sticky=(N, W, E, S)
        )
        i = 1
        # get local attributes
        for x in self.attributes:
            ttk.Button(
                self.attr_frame, text=x, command=partial(self.add_attribute, x)
            ).grid(row=i, column=0, sticky=(N, W, E, S))
            i = i + 1
        # get global attributes
        ttk.Label(self.attr_frame, text="Global Attributes:").grid(
            column=1, row=0, sticky=(N, W, E, S)
        )
        i = 1
        for x in attr.globalAttr:
            ttk.Button(
                self.attr_frame, text=x, command=partial(self.add_global, x)
            ).grid(row=i, column=1, sticky=(N, W, E, S))
            i = i + 1

    def add_attribute(self, textAttr):
        if "text:" in attr.tagAttr[textAttr]["value"]:
            response = attr.tagAttr[textAttr]["value"]
            try:
                text = ", :" + textAttr + " => '" + response + "'"
                self.t1.insert("thismark", text)
            except:
                text = "{ :" + textAttr + " => '" + response + "' }"
                self.t1.insert(END, text)
                self.t1.mark_set("thismark", "end - 3c")
        elif type(attr.tagAttr[textAttr]["value"]) is str:
            response = attr.tagAttr[textAttr]["value"]
            try:
                text = ", :" + textAttr + " => '" + response + "'"
                self.t1.insert("thismark", text)
            except:
                text = "{ :" + textAttr + " => '" + response + "' }"
                self.t1.insert(END, text)
                self.t1.mark_set("thismark", "end - 3c")
        else:
            try:
                text = ", :" + textAttr + " => "
                self.t1.insert("thismark", text)
                self.open_values_window(textAttr)
            except:
                text = "{ :" + textAttr + " => "
                self.t1.insert(END, text)
                self.open_values_window(textAttr)

    def add_global(self, textAttr):
        try:
            text = ", :" + textAttr + " => 'INSERT TEXT'"
            self.t1.insert("thismark", text)
        except:
            text = "{ :" + textAttr + " => 'INSERT TEXT' }"
            self.t1.insert(END, text)
            self.t1.mark_set("thismark", "end - 3c")

    def open_values_window(self, textAttr):
        self.v = Toplevel(self.c)
        i = 1
        ttk.Label(self.v, text="Select Attribute Value For " + textAttr + ": ").grid(
            column=0, row=0, sticky=(N, W, E, S)
        )
        for each in attr.tagAttr[textAttr]["value"]:
            ttk.Button(self.v, text=each, command=partial(self.add_value, each)).grid(
                row=i, column=0, sticky=(N, W, E, S)
            )
            i = i + 1
        ttk.Button(self.v, text="Abort", command=self.abort_v).grid(
            row=999, column=0, sticky=(N, W, E, S)
        )
        self.v.mainloop()

    def abort_v(self):
        self.v.destroy()

    def add_value(self, x):
        try:
            text = "'" + x + "'"
            self.t1.insert("thismark", text)
        except:
            text = "'" + x + "' }"
            self.t1.insert(END, text)
            self.t1.mark_set("thismark", "end -3c")
        self.v.destroy()
