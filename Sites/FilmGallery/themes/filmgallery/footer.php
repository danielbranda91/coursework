    <!-- Footer -->
    <footer class="footer">
      <div class="footer__logo-box">
        <picture class="footer__logo">
          <img src="https://via.placeholder.com/280x132.png?text=Logo+Here" alt="">
        </picture>
        <div class="footer__social u-center-text">
          <ul class="footer__social-list">
            <li>
              <a href="#" target="_blank">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
            <li>
              <a href="#" target="_blank">
                <i class="fab fa-vimeo-v"></i>
              </a>
            </li>
            <li>
              <a href="#" target="_blank">
                <i class="fab fa-linkedin-in"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>

      <div class="row">
        <div class="col-1-of-2">
          <div class="footer__navigation">
            <ul>
                <li><a href="#">Company</a></li>
                <li><a href="#">Contact us</a></li>
                <li><a href="#">Careers</a></li>
                <li><a href="#">Privacy policy</a></li>
                <li><a href="#">Terms</a></li>
            </ul>
          </div>
        </div>
        <div class="col-1-of-2">
          <p class="footer__copyright">
            Copyright &copy; by Portfolio Company
          </p>
        </div>
      </div>
    </footer>
    <div class="scroll-wrapper--up">
      <div class="btn btn--scroll">
        <a class="scroll-text" href="#top">
          &uarr;
        </a>
      </div>
    </div>

    <?php wp_footer(); ?>
  </body>
</html>