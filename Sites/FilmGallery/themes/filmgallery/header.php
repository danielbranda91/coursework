<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
  </head>

  <body>
    <header>
      <nav>
        <div class="navbar">
          <div class="fullrow">
            <div class="navbar__header">
              <!-- Button -->
              <div class="nav-mobile">
                <input type="checkbox" class="nav-mobile__checkbox" id="nav-toggle">
                <label for="nav-toggle" class="nav-mobile__button">
                  <span class="nav-mobile__icon">&nbsp;</span>
                </label>

                <!-- Mobile Menu background -->
                <div class="nav-mobile__background">&nbsp;</div>

                <div class="nav-mobile__nav">
                  <ul class="nav-mobile__list">
                    <li class="nav-mobile__item">
                      <a href="#section1" class="nav-mobile__link">Section 1</a>
                    </li>
                    <li class="nav-mobile__item">
                      <a href="#section2" class="nav-mobile__link">Section 2</a>
                    </li>
                    <li class="nav-mobile__item">
                      <a href="#section3" class="nav-mobile__link">Section 3</a>
                    </li>
                    <li class="nav-mobile__item">
                      <a href="#section3" class="nav-mobile__link">Section 4</a>
                    </li>
                    <li class="nav-mobile__item">
                      <a href="#contact" class="nav-mobile__link">Contact</a>
                    </li>
                  </ul>
                  <div class="nav-mobile__social">
                    <ul>
                      <li>
                        <a href="#" target="_blank">
                          <i class="fab fa-facebook-f"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#" target="_blank">
                          <i class="fab fa-vimeo-v"></i>
                        </a>
                      </li>
                      <li>
                        <a href="#" target="_blank">
                          <i class="fab fa-linkedin-in"></i>
                        </a>
                      </li>
                    </ul>
                  </div>
                  <!--End Navbar Nav  -->
                </div>

              </div>

              <div class="logo">
                <a href="<?php echo get_site_url();?>">
                  <img src="https://via.placeholder.com/280x132.png?text=Logo+Here" alt="Logo">
                </a>
              </div>
            </div>
            <!-- Navigation -->
            <div class="navbar__wrapper u-center-text">
              <ul class="navbar__list">
                <li>
                  <a href="#section1">Section 1</a>
                </li>
                <li>
                  <a href="#section2">Section 2</a>
                </li>
                <li>
                  <a href="#section3">Section 3</a>
                </li>
                <li>
                  <a href="#section4">Section 4</a>
                </li>
                <li>
                  <a href="#contact">Contact</a>
                </li>
              </ul>
            </div>
            <div class="navbar__social u-center-text">
              <ul class="navbar__social-list">
                <li>
                  <a href="#" target="_blank">
                    <i class="fab fa-facebook-f"></i>
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    <i class="fab fa-vimeo-v"></i>
                  </a>
                </li>
                <li>
                  <a href="#" target="_blank">
                    <i class="fab fa-linkedin-in"></i>
                  </a>
                </li>
              </ul>
            </div>

          </div>
        </div>
      </nav>
    </header>


        <!-- Social Links -->
<!-- <ul>
  
</ul> -->
