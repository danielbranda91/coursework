<?php

function portfolio_files() {
  wp_enqueue_script('main-portfolio-js', get_theme_file_uri('/js/scripts-bundled.js'), NULL, microtime(), true);
  wp_enqueue_style('google-custom-fonts', '//fonts.googleapis.com/css?family=Lato|Raleway&display=swap');
  wp_enqueue_style('font-awesome', '//use.fontawesome.com/releases/v5.8.2/css/all.css');
  wp_enqueue_style('portfolio_main_styles',get_stylesheet_uri());
}

add_action('wp_enqueue_scripts','portfolio_files');