<?php get_header(); ?>

<!-- Hero -->
<div class="hero" id="top">
  <!-- Video Background -->
  <div class="bg-video">
    <video class="bg-video__content" autoplay muted loop>
      <source src="<?php echo get_theme_file_uri('img/video.mp4');?>" type="video/mp4">
      Your browser is not supported!
    </video>
  </div>
  <!-- Text Rotation -->
  <div class="hero__text-box">
    <h1 class="heading-primary">
      <span class="heading-primary--main">Lorem Ipsum</span>
      <span class="heading-primary--sub">Illo expedita explicabo nobis?</span>
    </h1>
    <!-- Button -->
    <a href="#VimeoLink" class="btn btn--white btn--animated">Play Trailer</a>
  </div>
  
  <!-- Scroll Button with Bounce Animation -->
  <div class="scroll-wrapper--down">
    <div class="btn btn--scroll">
      <a class="scroll-text" href="#intro">
        &darr;
      </a>
    </div>
  </div>

<!-- End Hero -->
</div>
  
<main>
  <!-- Introduction Section -->

  <section class="section-intro" id="intro">
    <div class="section-intro__content">
      <div class="u-center-text u-margin-bottom-big">
        <h2 class="heading-secondary">Company Name</h2>
      </div>

      <div class="row">
        <div class="col-1-of-2">
          <h3 class="heading-tertiary u-margin-bottom-small">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. 
          </h3>
          <p class="paragraph">
            Vel totam laudantium quis, laboriosam deleniti expedita maiores cumque eaque at libero, impedit illo nisi alias aspernatur reiciendis minus perferendis. Maxime, corporis.Sapiente eaque eum, molestiae vero voluptatem, nemo repellat veniam eligendi repudiandae saepe autem quae exercitationem alias quidem, aliquam quasi tempora magni neque corrupti nisi? Provident ad excepturi quaerat est ipsa?
          </p>
          <h3 class="heading-tertiary u-margin-bottom-small">
            Lorem ipsum dolor sit!
          </h3>
          <p class="paragraph">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Eligendi, possimus voluptatem dolores accusamus similique fugiat!</p>
          <a href="#contact" class="btn-text">Contact Us &rarr;</a>
        </div>
        
        <div class="col-1-of-2">
          <div class="composition">
            <img src="<?php echo get_theme_file_uri('img/image1.jpg');?>" alt="" class="composition__photo composition__photo--p1">
            <img src="<?php echo get_theme_file_uri('img/image2.jpg');?>" alt="" class="composition__photo composition__photo--p2">
            <img src="<?php echo get_theme_file_uri('img/image3.jpg');?>" alt="" class="composition__photo composition__photo--p3">
          </div>
        </div>

      </div>
    </div>
</section>

  <!-- Pitch Section -->
<section class="section-pitch">
  <div class="section-pitch__content" style="background-image: url('<?php echo get_theme_file_uri('img/santorini.jpg');?>')">
    <div class="u-center-text u-margin-bottom-big">
      <h2 class="heading-secondary heading-secondary__white">Section Header</h2>
    </div>
    <!-- Iconography -->
    <div class="row">
      <div class="col-1-of-4">
        <div class="pitch-box">
          <i class="fas fa-chess-king u-center-text"></i>
          <h3 class="heading-tertiary__white u-center-text u-margin-bottom-small">Lorem</h3>
        </div>
      </div>
      <div class="col-1-of-4">
        <div class="pitch-box">
          <i class="fas fa-chess-queen"></i>
          <h3 class="heading-tertiary__white u-center-text u-margin-bottom-small">Ipsum</h3>
        </div>
      </div>
      <div class="col-1-of-4">
        <div class="pitch-box">
          <i class="fas fa-chess-knight u-center-text"></i>
          <h3 class="heading-tertiary__white u-center-text u-margin-bottom-small">Dolor</h3>
        </div>
      </div>
      <div class="col-1-of-4">
        <div class="pitch-box">
          <i class="fas fa-chess-bishop u-center-text"></i>
          <h3 class="heading-tertiary__white u-center-text u-margin-bottom-small">Sit</h3>
        </div>
      </div>
    </div>
  </div>
</section>

  <!-- Section 1: Services -->
  <section class="section-services" id="section1">
    <div class="section-services__content">
      <div class="u-center-text u-margin-bottom-big">
        <h2 class="heading-secondary heading-secondary__secondary-dark">Section Title</h2>
      </div>
      <!-- Service Boxes -->
      <div class="row">
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <h3>Dolor Sit</h3>
          <p class="paragraph">Voluptate veniam corporis obcaecati rem fugit atque numquam! Non consequuntur minima ab ex sunt molestiae inventore fugiat harum quia?</p>
        </div> 
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <h3>Dolor Sit</h3>
          <p class="paragraph">Voluptate veniam corporis obcaecati rem fugit atque numquam! Non consequuntur minima ab ex sunt molestiae inventore fugiat harum quia?</p>
        </div> 
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <h3>Dolor Sit</h3>
          <p class="paragraph">Voluptate veniam corporis obcaecati rem fugit atque numquam! Non consequuntur minima ab ex sunt molestiae inventore fugiat harum quia?</p>
        </div> 
      </div>
      <div class="row">
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <h3>Dolor Sit</h3>
          <p class="paragraph">Voluptate veniam corporis obcaecati rem fugit atque numquam! Non consequuntur minima ab ex sunt molestiae inventore fugiat harum quia?</p>
        </div> 
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <h3>Dolor Sit</h3>
          <p class="paragraph">Voluptate veniam corporis obcaecati rem fugit atque numquam! Non consequuntur minima ab ex sunt molestiae inventore fugiat harum quia?</p>
        </div> 
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <h3>Dolor Sit</h3>
          <p class="paragraph">Voluptate veniam corporis obcaecati rem fugit atque numquam! Non consequuntur minima ab ex sunt molestiae inventore fugiat harum quia?</p>
        </div> 
      </div>

    </div>
  </section>

  <!-- Section 2: Portfolio -->
  <section class="section-portfolio" id="section2">
    <div class="section-portfolio__content">
      <div class="u-center-text u-margin-bottom-big">
        <h2 class="heading-secondary heading-secondary__primary-color">Portfolio</h2>
      </div>
      <div class="row">
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <figure class="portfolio-card__frame">
            <img class="portfolio-card__image" src="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>" alt="">
            <figcaption class="portfolio-card__caption">
              <a href="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>">
                <i class="fas fa-search-plus"></i>
              </a>
            </figcaption>
          </figure>
          <div>
            <h3>Category</h3>
          </div>
        </div>
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <figure class="portfolio-card__frame">
            <img class="portfolio-card__image" src="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>" alt="">
            <figcaption class="portfolio-card__caption">
              <a href="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>">
                <i class="fas fa-search-plus"></i>
              </a>
            </figcaption>
          </figure>
          <div>
            <h3>Category</h3>
          </div>
        </div>
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <figure class="portfolio-card__frame">
            <img class="portfolio-card__image" src="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>" alt="">
            <figcaption class="portfolio-card__caption">
              <a href="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>">
                <i class="fas fa-search-plus"></i>
              </a>
            </figcaption>
          </figure>
          <div>
            <h3>Category</h3>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <figure class="portfolio-card__frame">
            <img class="portfolio-card__image" src="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>" alt="">
            <figcaption class="portfolio-card__caption">
              <a href="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>">
                <i class="fas fa-search-plus"></i>
              </a>
            </figcaption>
          </figure>
          <div>
            <h3>Category</h3>
          </div>
        </div>
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <figure class="portfolio-card__frame">
            <img class="portfolio-card__image" src="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>" alt="">
            <figcaption class="portfolio-card__caption">
              <a href="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>">
                <i class="fas fa-search-plus"></i>
              </a>
            </figcaption>
          </figure>
          <div>
            <h3>Category</h3>
          </div>
        </div>
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <figure class="portfolio-card__frame">
            <img class="portfolio-card__image" src="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>" alt="">
            <figcaption class="portfolio-card__caption">
              <a href="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>">
                <i class="fas fa-search-plus"></i>
              </a>
            </figcaption>
          </figure>
          <div>
            <h3>Category</h3>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <figure class="portfolio-card__frame">
            <img class="portfolio-card__image" src="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>" alt="">
            <figcaption class="portfolio-card__caption">
              <a href="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>">
                <i class="fas fa-search-plus"></i>
              </a>
            </figcaption>
          </figure>
          <div>
            <h3>Category</h3>
          </div>
        </div>
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <figure class="portfolio-card__frame">
            <img class="portfolio-card__image" src="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>" alt="">
            <figcaption class="portfolio-card__caption">
              <a href="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>">
                <i class="fas fa-search-plus"></i>
              </a>
            </figcaption>
          </figure>
          <div>
            <h3>Category</h3>
          </div>
        </div>
        <div class="col-1-of-3 u-margin-bottom-medium u-center-text">
          <figure class="portfolio-card__frame">
            <img class="portfolio-card__image" src="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>" alt="">
            <figcaption class="portfolio-card__caption">
              <a href="<?php echo get_theme_file_uri('img/portfolio-sample.jpg');?>">
                <i class="fas fa-search-plus"></i>
              </a>
            </figcaption>
          </figure>
          <div>
            <h3>Category</h3>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section-testimony">
    <div class="section-testimony__content" style="background-image: url('<?php echo get_theme_file_uri('img/image1.jpg');?>')">
    <!-- Note: Add Carousel Fx -->
      <div class="row">
        <div class="testimony-box u-center-text u-margin-bottom-big">
          <div class="testimony-box__slide">
            <i class="fas fa-quote-left u-margin-bottom-medium"></i>
            <p class="testimony-box__text u-margin-bottom-medium">Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque, commodi, illo vero, dignissimos odit eaque accusantium sunt nam necessitatibus excepturi facere ut animi quibusdam hic unde velit! Dolorum, pariatur quo.</p>
            <h6 class="testimony-box__name">Attribution Name</h6>
            <span class="testimony-box__assoc">Attribution Company</span> 
          </div>
          <div class="testimony-box__slide">
            <i class="fas fa-quote-left u-margin-bottom-medium"></i>
            <p class="testimony-box__text u-margin-bottom-medium">Odit voluptatem, culpa laborum accusamus quidem harum voluptates quos dignissimos aspernatur saepe aliquid, illum non maxime inventore facere modi ipsam dolor tenetur ratione totam? Dolores quam exercitationem iusto perspiciatis veritatis!</p>
            <h6 class="testimony-box__name">Attribution Name</h6>
            <span class="testimony-box__assoc">Attribution Company</span> 
          </div>
          <div class="testimony-box__slide">
            <i class="fas fa-quote-left u-margin-bottom-medium"></i>
            <p class="testimony-box__text u-margin-bottom-medium">Facere est odio nobis, pariatur, sequi, beatae laudantium rerum voluptatum fuga porro veniam doloremque unde id sapiente aut commodi excepturi nemo. Voluptates saepe animi ipsa provident sequi ea eius architecto!</p>
            <h6 class="testimony-box__name">Attribution Name</h6>
            <span class="testimony-box__assoc">Attribution Company</span> 
          </div>
        </div>
      </div>
    </div>
  </section>


  <!-- Section 3: Our Team -->
  <section class="section-biography" id="section3">
    <div class="section-biography__content">
      <div class="u-center-text u-margin-bottom-big">
        <h2 class="heading-secondary">About Us</h2>
      </div>
      <div class="row">
        <div class="col-1-of-4">&nbsp;</div>
        <div class="col-1-of-4 u-margin-bottom-medium u-center-text">
          <figure class="biography">
            <img src="<?php echo get_theme_file_uri('img/image4.jpg');?>" alt="" class="biography__image">
            <figcaption class="biography__caption">
              <h5>Job Title or Headline</h5>
              <a href="#LinkedIn" target="_blank">
                <i class="fab fa-linkedin-in"></i>
              </a>
              <a href="#email" target="_blank">
                <i class="fas fa-envelope"></i>
              </a>
            </figcaption>
          </figure>
          <div>
            <h3>Employee Name</h3>
          </div>
        </div>
        <div class="col-1-of-4 u-margin-bottom-medium u-center-text">
          <figure class="biography">
            <img src="<?php echo get_theme_file_uri('img/image5.jpg');?>" alt="" class="biography__image">
            <figcaption class="biography__caption">
              <h5>Job Title or Headline</h5>
              <a href="#LinkedIn" target="_blank">
                <i class="fab fa-linkedin-in"></i>
              </a>
              <a href="#email" target="_blank">
                <i class="fas fa-envelope"></i>
              </a>
            </figcaption>
          </figure>
          <div>
            <h3>Employee Name</h3>
          </div>
        </div>
        <div class="col-1-of-4">&nbsp;</div>
      </div>
    </div>
  </section>

  <!-- Section 4: Blog Carousel -->
  <section class="section-blog" id="section4">
    <div class="section-blog__content">
      <div class="u-center-text u-margin-bottom-big">
        <h2 class="heading-secondary heading-secondary__secondary-dark">Blog Title</h2>
      </div>
      <div class="row u-margin-bottom-small">
        <div class="col-1-of-2 u-center-text">
          <figure class="blog-post">
            <img src="<?php echo get_theme_file_uri('img/portfolio-sample.jpg'); ?>" alt="" class="blog-post__image">
          </figure>
          <div class="blog-post__title">
            <h3><a>Blog Title</a></h3>
          </div>
        </div>
        <div class="col-1-of-2 u-center-text">
          <figure class="blog-post">
            <img src="<?php echo get_theme_file_uri('img/portfolio-sample.jpg'); ?>" alt="" class="blog-post__image">
          </figure>
          <div class="blog-post__title">
            <h3><a>Blog Title</a></h3>
          </div>
        </div>
      </div>
      <footer class="u-center-text">
        <a class="btn-text btn-text__secondary btn-text__big" href="#">&larr;</a>
        <a class="btn-text btn-text__secondary btn-text__big" href="#">&rarr;</a>
      </footer>
    </div>
  </section>

  <section class="section-contact" id="contact">
    <div class="section-contact__content contact">
      <div class="u-center-text u-margin-bottom-big">
        <h2 class="heading-secondary heading-secondary__white">Contact Us</h2>
        <p class="paragraph paragraph__white">Repudiandae dignissimos laborum suscipit sunt iure atque, itaque quaerat libero maxime totam optio?</p>
      </div>
      <div class="row">
        <div class="col-1-of-3">
          <div class="contact__info">
            <span>
              <h6><i class="fas fa-mobile-alt"></i>Call Us: <span><a href="#">(555) 555-5555</a></span></h6>
            </span>
            <span>
              <h6><i class="fas fa-envelope"></i>Email Us: <span><a href="#">email@thisdomain.com</a></span></h6>
            </span>
          </div>
        </div>
        <div class="col-2-of-3">
          <div class="contact__form">
            <form action="">
              <div class="form__group">
                <input class="form__input" type="text" name="name" id="name" placeholder="Your Name">
                <label class="form__label" for="name" class="form__label">Your Name</label>
              </div>

              <div class="form__group">
                <input class="form__input" type="email" name="email" id="email" placeholder="Your Email Address">
                <label class="form__label" for="email">Email Address</label>
              </div>

              <div class="form__group">
                <input class="form__input" type="text" name="subject" id="subject" placeholder="Message Subject">
                <label class="form__label" for="subject">Message Subject</label>
              </div>
              
              <div class="form__group">
                <textarea class="form__input" name="comment" id="comment" placeholder="Message"></textarea>
                <label class="form__label" for="comment">Your Message</label>
              </div>

              <div class="form__group">
                <button class="btn btn--primary">Contact Us &rarr;</button>
              </div>
              <!-- Insert Confirmation message -->
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Scroll to Top: INCORP JAVASCRIPT SCROLL AND MOVE TO INDEX -->
</main>
<?php get_footer(); ?>

    

