// 3rd party packages from NPM
import $ from 'jquery';
import slick from 'slick-carousel';

// Our modules / classes
// import ClassExample from './modules/file';
import Scroll from './modules/Scroll';
import BtnScroll from './modules/BtnScroll';
import TestSlide from './modules/TestSlide';

// Instantiate a new object using our modules/classes
// var classExample = new ClassExample();
var scroll = new Scroll();
var btnScroll = new BtnScroll();
var testSlide = new TestSlide();
