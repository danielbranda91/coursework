import $ from 'jquery';

class Scroll {
  constructor() {
    this.doc = $(document);
    this.initScroll();
  }

  //methods
  initScroll() {
    this.doc.ready(function() {
      // Add smooth scrolling to all links
      $('a').on('click', function(event) {
        if (this.hash !== '') {
          event.preventDefault();

          var hash = this.hash;

          // use jquery's animate() to smoothly scroll to specified area in 800 milliseconds
          $('html, body').animate(
            {
              scrollTop: $(hash).offset().top
            },
            800,
            function() {
              window.location.hash = hash;
            }
          );
        }
      });
    });
  }
}

export default Scroll;
