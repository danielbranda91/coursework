import $ from 'jquery';

class TestSlide {
  constructor() {
    this.els = $('.testimony-box');
    this.initSlider();
  }

  initSlider() {
    this.els.slick({
      autoplay: true,
      // autoplaySpeed: 3000,
      arrows: false,
      dots: true
    });
  }
}

export default TestSlide;
