import $ from 'jquery';

class BtnScroll {
  constructor() {
    this.heroDiv = $('.hero');
    this.mainDiv = $('main');
    this.btnScrollDown = $('.scroll-wrapper--down');
    this.btnScrollUp = $('.scroll-wrapper--up');
    this.events();
    this.isScrollDownOpen = true;
    this.isScrollUpOpen = false;
  }

  events() {
    this.mainDiv.on('mouseenter', this.openScrollUp.bind(this));
    this.heroDiv.on('mouseenter', this.openScrollDown.bind(this));
  }

  // Methods
  openScrollUp() {
    if (this.isScrollDownOpen) {
      this.btnScrollUp.css('display', 'inline-block');
      this.btnScrollDown.css('display', 'none');
      this.isScrollUpOpen = true;
      this.isScrollDownOpen = false;
    }
  }

  openScrollDown() {
    if (!this.isScrollDownOpen) {
      this.btnScrollDown.css('display', 'inline-block');
      this.btnScrollUp.css('display', 'none');
      this.isScrollUpOpen = false;
      this.isScrollDownOpen = true;
    }
  }
}

export default BtnScroll;
