// 3rd party packages from NPM
import $ from 'jquery';

// Our modules / classes
import GoogleMap from './modules/GoogleMap';

// Instantiate a new object using our modules/classes
var googleMap = new GoogleMap();
