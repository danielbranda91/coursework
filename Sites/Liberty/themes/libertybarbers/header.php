<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php wp_head(); ?>
</head>

<body>

<!-- Elements -->
<!-- Background -->
<?php if(is_front_page()){ ?> 
  <section id="intro" style="background-image: url('<?php echo get_theme_file_uri("img/scissors-806238_75.png"); ?>'); background-repeat: repeat;">
<?php } ?>

<div class="row u-center-text">
  <div class="logo">
    <a href="<?php echo site_url(); ?>"><img src="<?php echo get_theme_file_uri('img/libertybarbersRBgold.jpg'); ?>" alt="Liberty Barbers"></a>
  </div>
</div>
<!-- Header -->
<header>
  <nav>
    <div class="navbar <?php if(is_user_logged_in()){echo 'admin-bar';} ?>">
      <div class="fullrow">

        <!-- Mobile Menu -->
        <div class="nav-mobile">
          <input type="checkbox" class="nav-mobile__checkbox" id="nav-toggle">
          <label for="nav-toggle" class="nav-mobile__button <?php if(is_user_logged_in()){echo 'admin-bar';} ?>">
            <i class="fa fa-bars expand"></i>
            <i class="fa fa-plus hide"></i>
          </label>
          <div class="nav-mobile__background <?php if(is_user_logged_in()){echo 'admin-bar-background';} ?>">&nbsp;
          </div>
          <div class="nav-mobile__nav">
            <?php 
              wp_nav_menu(array(
                'theme_location' => 'mobileMenuLocation'
              ));
            ?>

            <div class="nav-mobile__social">
              <ul>
                <li>
                  <a href="https://www.facebook.com/Liberty_barbers_nola-2047637055449050/" target="_blank">
                    <i class="fab fa-facebook-f"></i>
                  </a>
                </li>
              </ul>
            </div>
            <div class="nav-mobile__admin">
              <ul>
                <?php if(is_user_logged_in()) { ?>
                  <li>
                    <a href="<?php echo wp_logout_url(); ?>">
                      <span class="site-header__avatar">
                        <?php echo get_avatar(get_current_user_id(),60); ?>
                      </span>
                      <span>Log Out</span>
                    </a>
                  </li>
                <?php } else { ?> 
                  <li>
                    <a href="<?php echo wp_login_url(); ?>">Login</a> 
                  </li>
                  <li>
                    <a href="<?php echo wp_registration_url();?>">Sign Up</a>
                  </li>
                <?php } ?>
              </ul>         
            </div>                

          </div>
              
          
        </div>

        <div class="navbar__wrapper u-center-text">
          <?php 
            wp_nav_menu(array(
              'theme_location' => 'headerMenuLocation'
            ));
          ?>
        </div>
        <div class="navbar__admin u-center-text">
          <ul>
          <?php if(is_user_logged_in()) { ?>
            <li>
              <a href="<?php echo wp_logout_url(); ?>">
                <span class="site-header__avatar">
                  <?php echo get_avatar(get_current_user_id(),60); ?>
                </span>
                <span>Log Out</span>
              </a>
            </li>
          <?php } else { ?> 
            <li>
              <a href="<?php echo wp_login_url(); ?>">Login</a> 
            </li>
            <li>
              <a href="<?php echo wp_registration_url();?>">Sign Up</a>
            </li>
          <?php } ?>
            <li>
              <a href="https://www.facebook.com/Liberty_barbers_nola-2047637055449050/" target="_blank">
                <i class="fab fa-facebook-f"></i>
              </a>
            </li>
          </ul>
        </div>

      </div>
    </div>
  </nav>
</header>
