<?php

get_header(); ?>

<div class="row">
  <h1>The Polished Turd</h1>
</div>
<div>
  <?php 
    while(have_posts()) {
      the_post();
    ?>
      <div>
      <p><a href="<?php comments_link(); ?>">
	      <?php
          printf( _nx( 'One Comment', '%1$s Comments', get_comments_number(), 'comments title', 'textdomain' ), number_format_i18n( 		get_comments_number() ) ); 
        ?>
      </a></p>
        <p>Posted on <?php the_time('M d, Y')?></p>
        <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <p>By <?php the_author_posts_link(); ?></p>
        <p><?php the_excerpt(); ?> ... <a href="<?php the_permalink(); ?>">Read More</a></p>
        <p>Category: <?php echo get_the_category_list(', ');?></p>
      </div>
    <?php }
    echo paginate_links();
  ?>
</div>

<?php get_footer(); ?>