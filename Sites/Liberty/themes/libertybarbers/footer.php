  <footer>
    <p>Copyright &copy; <?php echo date ("Y"); ?> · All Rights Reserved · Liberty Barbers</p>
    <nav>
      <div class="foot-nav">
        <?php 
          wp_nav_menu(array(
            'theme_location' => 'footerMenuLocation'
          ));
        ?>
      </div>
    </nav>
    <div class="acf-map">
      <?php 
        $homepageLoc = new WP_Query(array(
          'posts_per_page' => 1,
          'post_type' => 'location'
        ));
        while($homepageLoc->have_posts()) {
          $homepageLoc->the_post();
          $mapLocation = get_field('shop_location'); ?>
          <div class="marker" data-lat="<?php echo $mapLocation['lat'];?>" data-lng="<?php echo $mapLocation['lng'];?>">
            <h3><?php the_title(); ?></h3>
            <?php echo $mapLocation['address'] ?>
          </div>
        <?php }
        wp_reset_postdata();
      ?>
    </div>
  </footer>

<?php wp_footer(); ?>
</body>
</html>

