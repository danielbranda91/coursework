<?php

get_header();

?>
    <div class="container">

        <div class="content">
          <div class="row banner">
            <div class="banner__image" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID(), "banner"); ?>');">
              <div class="banner__text-box">
                <h1><?php echo get_bloginfo('name'); ?></h1>
                <h2><?php echo get_bloginfo('description'); ?></h2>
              </div>
            </div>
          </div>
          <div class="banner__portable">
            <h1><?php echo get_bloginfo('name'); ?></h1>
            <h2><?php echo get_bloginfo('description'); ?></h2>
          </div>
          <div class="u-margin-bottom-medium">
            <?php echo get_field('pitch')?>
          </div>
          <div>  
            <div>
              <h2 class="heading-secondary heading-secondary__primary-color">The Polished Turd</h2>
              <p class="paragraph">Join the discussion at Liberty Barber's virtual barber stool, the Polished Turd.</p>
              <!-- Blog Calls -->
              <div class="row">

                <?php 
                  $homepagePosts = new WP_Query(array(
                    'posts_per_page' => 2,
                    'category_name' => 'The Polished Turd',
                  ));
                  
                  while ($homepagePosts -> have_posts()) {
                    $homepagePosts->the_post(); ?>
                    <!-- Post Card -->
                    <div class="col-1-of-2 u-margin-bottom-medium u-center-text">
                      <figure class="blog-card__frame">
                        <img class="blog-card__image"src="<?php echo get_theme_file_uri('img/jazz-1658886.jpg');?>" alt="">
                        <figcaption class="blog-card__caption">
                          <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </figcaption>
                      </figure>

                    </div>
                  <?php }; 
                ?>
              </div>


              <a class="btn-text" href="<?php echo site_url('/the-polished-turd'); ?>">Have a seat on the Polished Turd &rarr;</a>
              <?php wp_reset_postdata(); ?>
            </div>
          </div>
        </div>

    </div>
</section>

<section id="menu">
  <div class="row">
    <h2 class="heading-secondary heading-secondary__secondary-color">Menu</h2>
  </div>
  <div class="row">

    <?php 
      $homepageMenu = new WP_Query(array(
        'posts_per_page' => 50,
        'post_type' => 'menu',
        'order' => 'ASC'
      ));
      while($homepageMenu->have_posts()) {
        $homepageMenu->the_post(); ?>
        <div class="col-1-of-3">
          <?php the_post_thumbnail('menuPortrait');?>
          <p><?php the_title()?>: <?php echo get_field('price');?></p>
        </div>
      <?php }
    
    wp_reset_postdata();
    ?>

  </div>
</section>

<section id="hours">
  <h2>Hours</h2>
  <?php
    $today = date('Ymd'); 
    $homepageHours = new WP_Query(array(
      'posts_per_page' => 1,
      'post_type' => 'hours',
      'meta_key' => 'description',
      'meta_query' => array(
        array(
          'key' => 'description',
          'compare' => '<=',
          'value' => $today,
          'type' => 'numeric'
        )
      ),
      'orderby' => 'meta_value_num',
      'order' => 'DESC'
    ));
    while($homepageHours->have_posts()) {
      $homepageHours->the_post(); 
      $effectiveDate = new DateTime(get_field('description'));
      ?>
      <p>Effective <?php echo $effectiveDate->format('l, F j, Y');?>:</p>
      <ul>
        <li>Monday <?php echo get_field('monday');?></li>
        <li>Tuesday <?php echo get_field('tuesday');?></li>
        <li>Wednesday <?php echo get_field('wednesday');?></li>
        <li>Thursday <?php echo get_field('thursday');?></li>
        <li>Friday <?php echo get_field('friday');?></li>
        <li>Saturday <?php echo get_field('saturday');?></li>
        <li>Sunday <?php echo get_field('sunday');?></li>
      </ul>
    <?php }
    wp_reset_postdata();
  ?>
</section>

<section id="about-us">
  <h2>About Chris</h2>
  <?php echo get_field('about'); ?>
</section>

<section id="contact">
  <h2>Contact Liberty Barbers</h2>
  <div>
    <?php echo get_field('contact_form'); ?>
  </div>
</section>

<?php
get_footer();

?>