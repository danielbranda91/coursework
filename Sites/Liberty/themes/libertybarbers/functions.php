<?php

function libertybarbers_files() {
  wp_enqueue_script('googleMap', '//maps.googleapis.com/maps/api/js?key=AIzaSyDh8xv70EsQXxFIbdk80jPgqskR4IXodqw', NULL, microtime(), true);
  wp_enqueue_script('libertybarbers-js', get_theme_file_uri('/js/scripts-bundled.js'), NULL, microtime(), true);
  wp_enqueue_style('google-custom-font','//fonts.googleapis.com/css?family=Dancing+Script|Lobster');
  wp_enqueue_style('font-awesome','//use.fontawesome.com/releases/v5.8.2/css/all.css');
  wp_enqueue_style('libertybarbers_main_styles', get_stylesheet_uri(), NULL, microtime());
}

add_action('wp_enqueue_scripts','libertybarbers_files');

function libertybarbers_features() {
  add_theme_support('title-tag');
  add_theme_support('post-thumbnails');
  add_image_size('menuPortrait', 150, 150, true);
  add_image_size('banner',1024,273,true);
}

add_action('after_setup_theme', 'libertybarbers_features');

register_nav_menu('headerMenuLocation', 'Header Menu Location');
register_nav_menu('mobileMenuLocation', 'Mobile Menu Location');
register_nav_menu('footerMenuLocation', 'Footer Menu Location');

function lets_enqueue_comments_reply() {
  if(get_option('thread_comments') == 1){
    wp_enqueue_script('comment-reply');
  }
}

add_action('comment_form_before','lets_enqueue_comments_reply');

function libertybarbersMapKey($api) {
  $api['key'] = 'AIzaSyDh8xv70EsQXxFIbdk80jPgqskR4IXodqw';
  return $api;
}

add_filter('acf/fields/google_map/api', 'libertybarbersMapKey');

// Subscribers redirected to Front Page
add_action('admin_init', 'redirectSubsToFrontend');

function redirectSubsToFrontend() {
  $ourCurrentUser = wp_get_current_user();
  if (count($ourCurrentUser->roles) == 1 AND $ourCurrentUser->roles[0] ==
  'subscriber') {
  wp_redirect(site_url('/'));
  exit;
  }
}

// Subscribers Do Not Have Admin Bar
add_action('wp_loaded', 'noSubsAdminBar');

function noSubsAdminBar() {
  $ourCurrentUser = wp_get_current_user();
  if (count($ourCurrentUser->roles) == 1 AND $ourCurrentUser->roles[0] ==
  'subscriber') {
  show_admin_bar(false);
  }
}

// Customer Login Screen
add_filter('login_headerurl', 'ourHeaderUrl');

function ourHeaderUrl() {
  return esc_url(site_url('/'));
}

add_action('login_enqueue_scripts', 'ourLoginCSS');

function ourLoginCss() {
  wp_enqueue_style('google-custom-font','//fonts.googleapis.com/css?family=Dancing+Script|Lobster');
  wp_enqueue_style('libertybarbers_main_styles', get_stylesheet_uri());
}

add_filter('login_headertitle', 'ourLoginTitle');

function ourLoginTitle() {
  return get_bloginfo('name');
}

// Allow automatic updates
add_filter('allow_dev_auto_core_updates','__return_true');
add_filter('allow_minor_auto_core_updates','__return_true');
add_filter('allow_major_auto_core_updates','__return_true');