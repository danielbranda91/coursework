<?php 

get_header();

while(have_posts()) {
  the_post(); ?>
  <div>
    <p><a href="">Leave a Comment</a></p>
    <p>Posted on <?php the_time('M d, Y')?></p>
    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    <p>By <?php the_author_posts_link(); ?></p>
    <?php the_content(); ?>
    <p>Category: <?php echo get_the_category_list(', ');?></p>
    <div>
      <?php previous_post_link(); ?>
      <?php next_post_link(); ?>
    </div>
  </div>
<?php 

// If comments are open or we have at least one comment, load up the comment template.
if ( comments_open()) {
  comments_template();
}

}

get_footer();

?>