<?php

function libertybarbers_post_types() {
  register_post_type('menu', array(
    'public' => true,
    'labels' => array(
      'name' => 'Menu',
      'add_new_item' => 'Add New Menu Item',
      'edit_item' => 'Edit Menu Item',
      'all_items' => 'Menu',
      'singular_name' => 'Menu Item'
    ),
    'supports' => array('title','thumbnail'),
    'menu_icon' => 'dashicons-cart'
  ));
  register_post_type('hours', array(
    'public' => true,
    'labels' => array(
      'name' => 'Hours',
      'add_new_item' => 'Add New Hours',
      'edit_item' => 'Edit Hours',
      'all_items' => 'All Hours',
      'singular_name' => 'Hours'
    ),
    'supports' => array('title'),
    'menu_icon' => 'dashicons-clock'
  ));
  register_post_type('location', array(
    'public' => true,
    'labels' => array(
      'name' => 'Shop Location',
      'add_new_item' => 'Add New Shop Location',
      'edit_item' => 'Edit Shop Location',
      'all_items' => 'Shop Locations',
      'singular_name' => 'Shop Location'
    ),
    'menu_icon' => 'dashicons-store'
  ));
}

add_action('init', 'libertybarbers_post_types');