<?php

function lawfirm_files() {
  wp_enqueue_script('main-lawfirm-js', get_theme_file_uri('/js/scripts-bundled.js'), NULL, microtime(), true);
  wp_enqueue_style('google-custom-fonts', '//fonts.googleapis.com/css?family=Lato|Raleway&display=swap');
  wp_enqueue_style('font-awesome', '//use.fontawesome.com/releases/v5.8.2/css/all.css');
  wp_enqueue_style('lawfirm_main_styles', get_stylesheet_uri(), NULL, microtime());
  wp_localize_script('main-lawfirm-js', 'lawfirmData', array(
    'root_url' => get_site_url()
  ));
}

add_action('wp_enqueue_scripts','lawfirm_files');

function lawfirm_features() {
  add_theme_support('title-tag');
  register_nav_menu('footerMenuLocation', 'Footer Menu Location');
  add_image_size('awardThumbnail', 150, 150, true);
  add_image_size('postcardThumbnail', 389, 264, true);
  add_image_size('singleHeader',1006,246,true);
}

add_action('after_setup_theme', 'lawfirm_features');

add_theme_support('post-thumbnails');