<?php

get_header();

while (have_posts()) {
  the_post();
?>
<main>
  <section class="subblank-cont">
    <div class="main wide content-panel">
      <h1><?php echo get_field('headline'); ?></h1>
      <h2><?php echo get_field('sub-headline'); ?></h2>
      <?php echo get_field('lead_paragraph'); ?>
      <p class="text-highlight-1"><strong><?php echo get_field('pitch'); ?></strong></p>
      <?php echo get_field('close_paragraph');  
} ?>
    </div>
  </section>
  <section class="main staff-list">
    <div class="atty-list">
      <header class="fancy main short">
        <h3>
          <strong>Practice Areas</strong>
        </h3>
      </header>
    </div>
    <div class="box-list attorneys">
      <ul class="total9">
        <?php 
          $practices = new WP_Query(array(
            'posts_per_page' => -1,
            'post_type' => 'practice',
            'post_parent' => 0
          ));
          while ($practices->have_posts()) {
            $practices->the_post(); ?>
            <li class="item1">
              <a href="<?php the_permalink(); ?>">
                <div class="pic-box">
                  <img src="<?php echo get_theme_file_uri('img/spacer.gif') ?>" alt="" style="background-image:url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'postcardThumbnail'); ?>)">
                  <span class="btn">View Practice Area</span>
                </div>
                <div class="info-btn">
                  <?php the_title(); ?>
                </div>
              </a>
            </li>
          <?php }
          wp_reset_postdata();
        ?>
      </ul>
    </div>
  </section>

  <?php get_template_part('template-parts/contact'); ?>
</main>

<?php

get_footer();

?>