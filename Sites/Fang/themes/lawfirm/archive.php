<?php get_header(); ?>
<!-- Body -->
<div class="cf main-contain">
  <div class="sidebox">
    <main>
      <div class="content-panel">
        <h1>Lawfirm News:</h1>
        <h2><?php the_archive_title(); ?></h2>
      </div>
      <div class="bloghome">
      <?php 
        while (have_posts()) {
          the_post(); ?>
          <div class="item blog">
            <div class="dateline">
              <?php the_time('M d, Y'); ?>
            </div>
            <h2>
              <a href="<?php the_permalink(); ?>" class="vertical-align:top"><?php the_title(); ?></a>
            </h2>
            <div class="content content-panel">
              <img src="#" alt="thumbnail" border="0" style="width:100px; margin-bottom:8px; float:left; margin-right:8px;">
              <span><?php wp_trim_words(get_the_content(),20); ?></span>
            </div>
            <div class="byline">
              <div>
                <span>Posted By: </span><?php the_author_posts_link(); ?>
              </div>
              <a href="<?php the_permalink(); ?>">Continue Reading &raquo;</a>
            </div>
            <div class="divider"></div>
            <div style="clear:both"></div>
          </div>

        <?php } 
        echo paginate_links();
      ?>
      </div>
    </main>
  </div>
  <aside class="side-container">
    <div>
      <?php
        $recentNews = new WP_Query(array(
          'posts_per_page' => 5
        ));
        if ($recentNews->have_posts()) { ?>
          <nav class="side-nav sys-nav">
            <header class="header">
              Recent Posts
            </header>
            <ul class="list menu">
              <!-- Content Loop -->
              <?php while ($recentNews->have_posts()) {
                $recentNews->the_post(); ?>
                <li class="level">
                  <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                  </a>
                </li>
              <?php } ?>
            </ul>
          </nav>
        <?php }
        wp_reset_postdata();
      ?>
    </div>
    <!-- Save for Future Date Archives -->
    <div>
      <?php   
        $archives = array(
          'type' => 'monthly',
          'post_type' => 'post'
        ); 
      ?>
      <nav class="side-nav sys-nav">
        <header class="header">
          Archives
        </header>
        <ul class="list menu">
          <?php
            $my_archives = wp_get_archives($archives);
            echo $my_archives; 
          ?>
        </ul>
      </nav>
    </div>
  </aside>
  <div>
  <?php get_template_part('template-parts/contact'); ?>
  </div>
</div>

<?php get_footer(); ?>
