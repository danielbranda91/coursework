<?php get_header(); ?>
<!-- Body -->

<main>
  <section class="animate-section">
    <div class="animation">
      <div class="slide">
        <img src="<?php echo get_theme_file_uri('img/spacer.gif') ?>" class="slide-bg" style="background-image:url('<?php echo get_field('banner_image')['url']; ?>')">
        <div class="main">
          <div class="info">
            <h1><?php echo get_bloginfo('description');?></h1>
            <h2>
              <em><?php echo get_field('top_slogan');?></em> <?php echo get_field('bottom_slogan'); ?>
            </h2>
            <a href="<?php echo site_url('/contact-us');?>" class="btn">Contact Us Today</a>
            <div class="content-panel animate-cont">
              <?php echo get_field('introduction');?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <section class="wide-cta-section main hide">
    <div class="wide-cta">
      <h3><?php echo get_field('wide_cta');?></h3>
    </div>
    <div class="btn-wrap">
      <a href="<?php echo site_url('/about-us');?>" class="btn">Learn More About Us</a>
    </div>
  </section>
  
  <section class="prac-acco-section" style="background-image:url('<?php echo get_field('second_banner_image')['url']; ?>')">
    <div class="main box">
      <nav class="practice-nav">
        <header>
          <h3>
            <a href="practice-areas.html">Cases We Handle</a>
          </h3>
        </header>
        <ul>
        <?php 
          $homepageCases = new WP_Query(array(
            'posts_per_page' => 10,
            'post_type' => 'practice',
            'post_parent' => 0
          ));
          while ($homepageCases->have_posts()) { 
            $homepageCases->the_post(); ?>
            <li class="level1">
              <a href="<?php the_permalink(); ?>" target>
                <?php the_title(); ?>
              </a>
            </li>
          <?php }
          wp_reset_postdata();
          ?>
        </ul>
        <footer class="btn-container">
          <a href="<?php echo site_url('/practice-areas');?>" class="btn">Learn More</a>
        </footer>
      </nav>
      <div class="accolades">
        <header>
          <h3>Accolades</h3>
        </header>
        <div class="award-list">
          <ul class="total6">
            <?php 
              $homepageAwards = new WP_Query(array(
                'posts_per_page' => 6,
                'post_type' => 'award'
              ));
              while ($homepageAwards->have_posts()) {
                $homepageAwards->the_post(); ?>
                <li class="award-box award1">
                  <?php the_post_thumbnail('awardThumbnail');?>
                  <div><?php the_title(); ?></div>
                </li>
              <?php }
              wp_reset_postdata();
            ?>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <section class="midcont-section">
    <div class="main box half">
      <div class="mid-img">
        <img src="<?php echo get_field('meet_team_image')['url']; ?>" style="width:100%;height:auto" width="541" height="361">
      </div>
      <div class="cont-wrap">
        <div class="content-panel overlap showhide show">
          <h2><?php echo get_field('meet_team_headline');?></h2>
          <?php echo get_field('meet_team_pitch');?>
          <a href="<?php echo site_url('/attorneys');?>" class="btn">Meet Our Team</a>
        </div>
      </div>
    </div>
  </section>

  <section class="results-section">
    <div class="main">
      <header>
        <h2>Case Results</h2>
      </header>
      <div class="box">
        <div class="results-info first">
          <div class="content">
            <h3>Lorem</h3>
            <h4>ipsum <em>dolor</em> sit.</h4>
            <p>Amet consectetur adipisicing elit. Quia, ut! Esse aliquam dolor cumque minus harum qui praesentium sapiente accusamus quidem laudantium dolorum tempore, inventore alias eos voluptatum saepe eius.</p>
          </div>
          <ul class="results-list">
            <?php
              $homepageResults = new WP_Query(array(
                'posts_per_page' => 4,
                'post_type' => 'result',
              ));
              while ($homepageResults->have_posts()) {
                  $homepageResults->the_post();
                  ?>
                  <li class="result-1">
                    <h2><?php echo get_field('case_settlement'); ?></h2>
                    <h4><?php the_title(); ?></h4>
                    <div class="hover">
                      <h4><?php the_title(); ?></h4>
                      <span><?php echo get_field('case_description');?></span>
                    </div>
                  </li>
                <?php
              } 
              wp_reset_postdata();
            ?>
          </ul>
        </div>
        <div class="results-info second">
          <div class="content">
            <h3>Lorem</h3>
            <h4>ipsum <em>dolor</em> sit.</h4>
            <p>Amet consectetur adipisicing elit. Quia, ut! Esse aliquam dolor cumque minus harum qui praesentium sapiente accusamus quidem laudantium dolorum tempore, inventore alias eos voluptatum saepe eius.</p>
          </div>
          <ul class="results-list">
          <?php
            $homepageResults = new WP_Query(array(
              'posts_per_page' => 4,
              'post_type' => 'result',
              'offset' => 4
            ));
            while ($homepageResults->have_posts()) {
                $homepageResults->the_post(); ?>
                <li class="result-1">
                  <h2><?php echo get_field('case_settlement'); ?></h2>
                  <h4><?php the_title(); ?></h4>
                  <div class="hover">
                    <h4><?php the_title(); ?></h4>
                    <span><?php echo get_field('case_description');?></span>
                  </div>
                </li>
              <?php }
              wp_reset_postdata();
            ?>
          </ul>
        </div>
      </div>
      <div class="btn-container">
        <a href="<?php echo site_url('/case-results');?>" class="btn">See Our Results</a>
      </div>
    </div>
  </section>

  <section class="value-section">
    <div class="box main">
      <div class="content">
        <header class="val-header fancy">
          <h3><strong>Values</strong> Headline</h3>
          <h4>Goes <strong>Right Here</strong></h4>
        </header>
        <div class="box">
          <div class="quote">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Nisi, dolores magni in enim et quod? <strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab, inventore!</strong>
            <em>
              <strong>Client Name</strong>
              client attribution
            </em>
          </div>
          <div class="btn-container value-btn">
            <a href="<?php echo site_url('/why-choose-us');?>" class="btn">Why Choose Us</a>
          </div>
        </div>
      </div>
      <ul class="value-list">
        <li class="value-1">
          <icon class="fontello money-recovered"></icon>
          <h4>Lorem ipsum dolor sit amet, consectetur adipisicing.</h4>
        </li>
        <li class="value-2">
          <icon class="fontello trial-lawyers"></icon>
          <h4>Lorem ipsum dolor sit amet, consectetur adipisicing.</h4>
        </li>
        <li class="value-3">
          <icon class="fontello excellence"></icon>
          <h4>Lorem ipsum dolor sit amet, consectetur adipisicing.</h4>
        </li>
        <li class="value-4">
          <icon class="fontello dedicated"></icon>
          <h4>Lorem ipsum dolor sit amet, consectetur adipisicing.</h4>
        </li>
      </ul>
    </div>
  </section>

  <?php get_template_part('template-parts/contact'); ?>

</main>

 <!-- End Main -->
<?php get_footer(); ?>

