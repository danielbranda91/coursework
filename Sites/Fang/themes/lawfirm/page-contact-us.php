<?php

  get_header();

  while (have_posts()) {
    the_post(); ?>
    <main>
      <section class="contact-pg">
        <div class="main">
          <div class="content-panel cf">
            <h1><?php the_title(); ?></h1>
            <?php the_content(); ?>
          </div>
      </section>
      <?php get_template_part('template-parts/contact'); ?>
  </main>


  <?php }

  get_footer();

?>