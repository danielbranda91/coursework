<form 
  <?php 
    if (!is_front_page()) {
      echo 'style="margin:0px;"';
  }
  ?>  
action="">
    <?php 
      if (is_front_page()) { ?>
        <section class="contact-news-section">
          <div class="main box">
            <div class="contact-box">
      <?php } else { ?>
        <section class="sub-form-section">
          <div class="contact-box">
            <div class="main short">
      <?php }
    ?>
          <header class="fancy">
            <h3>Invitation <strong>For Contact</strong></h3>
            <h4>Pitch <strong>Goes Here</strong></h4>
          </header>
          <div class="box">
            <div>
              <p>Lorem ipsum dolor sit amet consectetur, <strong>adipisicing elit. Ex!</strong></p>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati, repudiandae perferendis! <strong>Dolorem animi laborum ipsam nostrum temporibus deleniti, non officia iste excepturi reiciendis consectetur expedita itaque beatae, mollitia porro architecto.</strong></p>
            </div>
            <div class="contact-form">
              <fieldset>
                <ul>
                  <li>
                    <label class="hide" for="name">Name</label>
                    <div class="input-text">
                      <input type="text" placeholder="Name" required="required" name="name" value>
                      <div class="validation" for="name" data-type="valueMissing">Please enter your name</div>
                    </div>
                  </li>
                  <li>
                    <label for="phone" class="hide">Phone</label>
                    <div class="input-text">
                      <input type="text" type="tel" placeholder="Phone" pattern="[(]\d{3}[)][\s]\d{3}[\-]\d{4}" class="phone-mask" required="required" name="phone" value>
                      <div class="validation" for="phone" data-type="typeMismatch">
                        This isn't a valid phone number.
                      </div>
                    </div>
                  </li>
                </ul>
                <ul>
                  <li>
                    <label for="email" class="hide">Email</label>
                    <div class="input-text">
                      <input type="input" placeholder="Email" required="required" name="email" value>
                      <div class="validation" for="email" data-type="typeMismatch">
                        This isn't a valid email address.
                      </div>
                    </div>
                  </li>
                  <li>
                    <label for="existing" class="hide">Existing Client?</label>
                    <div class="input-text">
                      <select name="existing" id="existingType" required="required">
                        <option value>Are you a new client?</option>
                        <option value="Yes">Yes, I am a potential new client</option>
                        <option value="No">No, I am an existing client</option>
                        <option value="Neither">I am neither</option>
                      </select>
                    </div>
                  </li>
                  <li>
                    <label for="message" class="hide">Message</label>
                    <div class="input-text">
                      <textarea name="message" id="message" placeholder="Message" required="required" type="text"></textarea>
                      <div class="validation" for="message" data-type="valueMissing">
                        Please enter a message.
                      </div>
                    </div>
                  </li>
                </ul>
              </fieldset>
              <div class="g-recaptcha">
                <!-- Recaptcha Goes Here -->
              </div>
              <div class="submit-btn">
                <button class="btn" type="submit">Contact Us</button>
              </div>
            </div>
          </div>
        </div>
        <?php 
          if (is_front_page()) { ?>

            <div class="blogfeed-box">
              <header>
                <h2>Our<br>News</h2>
              </header>
              <?php 
                $homepageNews = new WP_Query(array(
                  'posts_per_page' => 2
                ));
                while ($homepageNews->have_posts()) {
                  echo '<ul>';
                  $homepageNews->the_post(); ?>
                  <li>
                    <a href="<?php the_permalink(); ?>" class="cf">
                      <div class="date"><?php the_time('M d, Y'); ?></div>
                      <div class="text">
                        <h3><?php the_title(); ?></h3>
                      </div>
                    </a>
                  </li>
                  <?php 
                  echo '</ul>';
                }
                wp_reset_postdata();
              ?>
              <footer class="btn-container">
                <a href="<?php echo site_url('/news');?>" class="btn">Visit Our Blog</a>
              </footer>
            </div>
          <?php }
        ?>
      </div>
    </section>
  </form>