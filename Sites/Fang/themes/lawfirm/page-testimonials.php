<?php
  get_header();
?>

<main>
  <div class="sbc banner">
    <img src="<?php echo get_field('banner')['url'];?>" width="2000" height="566">
    <div class="main">
      <div class="sbc-text">
        <h1><?php the_title(); ?></h1>
      </div>
    </div>
  </div>
  <section class="content-one cont-over content-boxes">
    <div class="main box">
      <div class="cont-wrap two-thirds">
        <div class="content-panel overlap showhide">
          <?php the_content(); ?>
        </div>
      </div>
      <div class="top-img third img-chk">  
      </div>
    </div>
  </section>
  <section class="test-sys">
    <header class="fancy main short">
      <h3>
        <strong>From Our Clients & Colleagues</strong>
      </h3>
    </header>
    <ul class="main">
      <?php
        $showTestimonials = new WP_Query(array(
          'posts_per_page' => -1,
          'post_type' => 'testimonial'
        ));
        while ($showTestimonials->have_posts()) {
          $showTestimonials->the_post(); ?>
          <li class="article showhide">
            <blockquote>
              <small>
                <span><?php echo get_bloginfo('name'); ?></span>
              </small>
              <div class="review">
                <h2>
                  <em>"</em>
                    <?php the_content(); ?>
                  <em>"</em>
                </h2>
              </div>
              <div class="test-info">
                <h3>
                  <strong>- <?php echo get_field('testimonial_name'); ?></strong>
                  <em><?php echo get_field('attribution');?></em>
                </h3>
              </div>
            </blockquote>
          </li>
        <?php }
        wp_reset_postdata();
      ?>



    </ul>
  </section>
  <?php get_template_part('template-parts/contact'); ?>
</main>

<?php 
  get_footer();
?>