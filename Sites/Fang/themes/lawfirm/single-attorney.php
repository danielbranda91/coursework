<?php
  get_header();
?>

<main>
  <section class="atty-header">
    <div class="box">
      <div class="atty-img">
        <img src="<?php echo get_theme_file_uri('img/spacer.gif') ?>" style="background-image:url('<?php echo get_the_post_thumbnail_url(); ?>')">
      </div>
      <div class="atty-info content-panel">
        <h1><?php the_title(); ?></h1>
        <h3><?php echo get_field('job_title'); ?></h3>
        <p>
          <strong>Practice Area(s):</strong>
          <span class="p-areas">List,List</span>
        </p>
      </div>
    </div>
  </section>
  <section class="atty-body">
    <div class="box">
      <nav class="atty-tabs"></nav>
      <div class="atty-body-content content-panel">
        <a href="#Biography" class="btn v1">Biography</a>
        <div id="Biography" class="active">
          <h2><?php echo get_field('biography_headline');?></h2>
          <?php echo get_field('biography');?>
        </div>
        <div>
          <a href="#" class="video-spotlight btn v1">Play Video</a>
        </div>
        <?php
          if (get_field('legal_approach')) { ?>
            <a href="#LegalApproach" class="btn v1">Legal Approach</a>
            <div id="LegalApproach">
              <?php echo get_field('legal_approach');?>
            </div>
          <?php }
        ?>
        <?php
          if (get_field('highlights')) { ?>
            <a href="#CareerHighlights" class="btn v1">Career Highlights</a>
            <div id="CareerHighlights">
              <?php echo get_field('highlights');?>
            </div>
          <?php }
        ?>

        <?php
          if (get_field('early_years')) { ?>
            <a href="#EarlyYears" class="btn v1">Early Years</a>
            <div id="EarlyYears">
              <?php echo get_field('early_years');?>
            </div>
          <?php }
        ?>
        <a href="#Education" class="btn v1">Education</a>
        <div id="Education">
          <?php echo get_field('education');?>
        </div>
      </div>
    </div>
  </section>
  <!-- Insert Video Modal -->
  <?php get_template_part('template-parts/contact'); ?>
</main>

<?php
  get_footer();
?>
