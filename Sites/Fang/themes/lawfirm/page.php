<?php

  get_header();

  while (have_posts()) {
    the_post(); ?>
    <main>
      <div class="sbc banner">
        <img src="<?php echo get_field('banner')['url'];?>">
        <div class="main">
          <div class="sbc-text">
            <h1><?php the_title(); ?></h1>
          </div>
        </div>
      </div>

      <section class="content-one cont-over content-boxes">
        <div class="main box">
          <div class="cont-wrap two-thirds">
            <div class="content-panel overlap showhide">
              <?php echo get_field('main_content'); ?>
            </div>
          </div>
          <?php 
            if (get_field('side_image_1')) { ?>
              <div class="top-img third img-chk">
                <span>
                  <img src="<?php echo get_field('side_image_1')['url'];?>" alt="Image Caption">
                </span>
              </div>
            <?php }
          ?>  
        </div>
      </section>

      <section class="content-two cont-over content-boxes">
        <?php 
          if (get_field('sub-banner')) { ?>
            <div class="banner-img-two">
              <img src="<?php echo get_field('sub-banner')['url']; ?>" width="2000" height="566">
            </div>
          <?php }

          if (get_field('sub_content')) { ?>
            <div class="main box">
              <?php 
                if (get_field('side_image_2')) { ?>
                  <div class="bottom-img third img-chk">
                    <span>
                      <img src="<?php echo get_field('side_image_2')['url']; ?>">
                    </span>
                  </div>
                <?php } ?>
              <div class="cont-wrap two-thirds">
                <div class="content-panel overlap showhide">
                  <?php echo get_field('sub_content'); ?>
                </div>
              </div>
            </div>
          <?php }
        ?>
      </section>
    <!-- Contact Section Form -->
    <?php get_template_part('template-parts/contact'); ?>
  </main>


  <?php }

  get_footer();

?>