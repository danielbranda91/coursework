<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <!-- Header -->
  <header>
    <div class="masthead full">
      <a href="<?php echo site_url('/');?>" class="top-logo">
        <div>
          <img src="https://via.placeholder.com/254x53" alt="Lawfirm" title="Lawfirm">
        </div>
      </a>
      <nav class="top-nav">
        <ul class="desktop-nav">
          <li>
            <a <?php if (is_front_page()) echo 'class="selected"';?> href="<?php echo site_url('/'); ?>" target="">
              <span>Home</span>
            </a>
          </li>
          <li>
            <a <?php if (is_page('about-us') or wp_get_post_parent_id(0) == 18) echo 'class="selected"'; ?> href="<?php echo site_url('/about-us'); ?>" target="">
              <span>About Us</span>
            </a>
            <div class="nav-flyout ui-scroll">
              <ul>
                <li>
                  <a href="<?php echo site_url('/our-history');?>">
                    <span>Our History</span>
                  </a>
                </li>
                <li>
                  <a href="<?php echo site_url('/why-choose-us'); ?>">
                    <span>Why Choose Us?</span>
                  </a>
                </li>
                <li>
                  <a href="<?php echo site_url('/corporate-social-responsibility'); ?>">
                    <span>Corporate Social Responsibility</span>
                  </a>
                </li>
                <li>
                  <a href="<?php echo site_url('/testimonials');?>">
                    <span>Testimonials</span>
                  </a>
                </li>
                <li>
                  <a href="<?php echo site_url('/careers');?>">
                    <span>Careers</span>
                  </a>
                </li>
                <li>
                  <a href="<?php echo site_url('/videos')?>">
                    <span>Videos</span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <!-- End About Us -->
          <li>
            <a <?php if (is_page('practice-areas') or get_post_type() == 'practice') echo 'class="selected"'; ?> href="<?php echo site_url('/practice-areas');?>" target="">
              <span>Practice Areas</span>
            </a>
            <div class="nav-flyout ui-scroll">
              <ul>
                <?php 
                  $headerPractices = new WP_Query(array(
                    'posts_per_page' => -1,
                    'post_type' => 'practice',
                    'post_parent' => 0
                  ));
                  while ($headerPractices->have_posts()) {
                    $headerPractices->the_post(); ?>
                    <li>
                      <a href="<?php the_permalink(); ?>">
                        <span><?php the_title(); ?></span>
                      </a>
                    </li>
                  <?php }
                  wp_reset_postdata(); 
                ?>
              </ul>
            </div>
          </li>
          <!-- End Practice Areas -->
          <li>
            <a <?php if (is_page('attorneys') or get_post_type() == 'attorney') echo 'class="selected"'; ?> href="<?php echo site_url('/attorneys'); ?>">
              <span>Our Team</span>
            </a>
            <div class="nav-flyout ui-scroll">
              <ul>
                <?php 
                  $headerAttorneys = new WP_Query(array(
                    'posts_per_page' => -1,
                    'post_type' => 'attorney',
                    'orderby' => 'menu_order',
                    'order' => 'ASC'
                  ));
                  while ($headerAttorneys->have_posts()) {
                    $headerAttorneys->the_post(); ?>
                    <li>
                      <a href="<?php the_permalink(); ?>">
                        <span><?php the_title(); ?></span>
                      </a>
                    </li>
                  <?php }
                  wp_reset_postdata(); 
                ?>
              </ul>
            </div>
          </li>
          <!-- End Our Team -->
          <li>
            <a <?php if (is_page('case-results') or get_post_type() == 'result') echo 'class="selected"'; ?> href="<?php echo site_url('/case-results');?>">
              <span>Case Results</span>
            </a>
            <div class="nav-flyout ui-scroll">
              <ul>
                <?php 
                  $headerResults = new WP_Query(array(
                    'posts_per_page' => 10,
                    'post_type' => 'result',
                  ));
                  while ($headerResults->have_posts()) {
                    $headerResults->the_post(); ?>
                    <li>
                      <a href="<?php the_permalink(); ?>">
                        <span><?php the_title(); ?></span>
                      </a>
                    </li>
                  <?php }
                  wp_reset_postdata(); 
                ?>
              </ul>
            </div>
          </li>
          <li>
            <a href="<?php echo site_url('/testimonials');?>">
              <span>Testimonials</span>
            </a>
          </li>
          <li>
            <a <?php if (get_post_type() == 'post') echo 'class="selected"' ?> href="<?php echo site_url('/news'); ?>">
              <span>News</span>
            </a>
          </li>
          <li>
            <a href="<?php echo site_url('/contact-us');?>">
              <span>Contact Us</span>
            </a>
          </li>
        </ul>
        <ul class="mobile-nav">
          <li class="mobile-menu active">
            <nav class="enter">
              <ul id="topMobileMenu">
                <li>
                  <a href="<?php echo site_url('/'); ?>">Home</a>
                </li>
                <li class="hasChild">
                  <a href="<?php echo site_url('/about-us'); ?>">About Us</a>
                  <ul class="childMenu">
                    <li>
                      <a href="<?php echo site_url('/our-history'); ?>">
                      Our History
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo site_url('/why-choose-us');?>">
                        Why Choose Us?
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo site_url('/corporate-social-responsibility'); ?>">
                        Corporate Social Responsibility
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo site_url('/testimonials');?>">
                        Testimonials
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo site_url('/careers');?>">
                        Careers
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo site_url('/videos')?>">
                        Videos
                      </a>
                    </li>
                  </ul>
                </li>
                <li class="hasChild">
                  <a href="<?php echo site_url('/practice-areas'); ?>">Practice Areas</a>
                  <ul class="childMenu">
                    <?php 
                      $headerPractices = new WP_Query(array(
                        'posts_per_page' => -1,
                        'post_type' => 'practice',
                        'post_parent' => 0
                      ));
                      while ($headerPractices->have_posts()) {
                        $headerPractices->the_post(); ?>
                        <li>
                          <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                          </a>
                        </li>
                      <?php }
                      wp_reset_postdata(); 
                    ?>
                  </ul>
                </li>
                <li class="hasChild">
                  <a href="<?php echo site_url('/attorneys');?>">Our Team</a>
                  <ul class="childMenu">
                    <?php 
                      $headerAttorneys = new WP_Query(array(
                        'posts_per_page' => -1,
                        'post_type' => 'attorney',
                        'orderby' => 'menu_order',
                        'order' => 'ASC'
                      ));
                      while ($headerAttorneys->have_posts()) {
                        $headerAttorneys->the_post(); ?>
                        <li>
                          <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                          </a>
                        </li>
                      <?php }
                      wp_reset_postdata(); 
                    ?>
                  </ul>
                </li>
                <li class="hasChild">
                  <a href="<?php echo site_url('/case-results');?>">Case Results</a>
                  <ul class="childMenu">
                    <?php 
                      $headerResults = new WP_Query(array(
                        'posts_per_page' => 10,
                        'post_type' => 'result',
                      ));
                      while ($headerResults->have_posts()) {
                        $headerResults->the_post(); ?>
                        <li>
                          <a href="<?php the_permalink(); ?>">
                            <?php the_title(); ?>
                          </a>
                        </li>
                      <?php }
                      wp_reset_postdata(); 
                    ?>
                  </ul>
                </li>
                <li>
                  <a href="<?php echo site_url('/news');?>">News</a>
                </li>
                <li>
                  <a href="<?php echo site_url('/contact-us');?>">Contact Us</a>
                </li>
              </ul>
              <div class="exit">
                <div class="close"></div>
              </div>
            </nav>
          </li>
        </ul>
      </nav>
      <div class="trig-wrap">
        <div class="menu-trigger">
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="close"></div>
        </div>
      </div>
      <div class="contact-info">
        <a href="#" class="phone-number">555.555.5555</a>
        <div class="mob-phone">
          <a href="#" class="mobphone-number">
            <i class="fas fa-mobile alt"></i>
          </a>
        </div>
      </div>
      <a class="search-trigger">
        <i class="fas fa-search"></i>
      </a>
    </div>
  </header>

