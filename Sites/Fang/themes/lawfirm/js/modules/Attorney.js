import $ from 'jquery';

class Attorney {
  constructor() {
    this.base = $('.atty-body-content');
    this.biography = $('#Biography');
    this.bioApproach = $('#LegalApproach');
    this.bioHighlights = $('#CareerHighlights');
    this.bioEarlyYears = $('#EarlyYears');
    this.bioEducation = $('#Education');
    this.btnBiography = $('a[href$="#Biography"]');
    this.btnApproach = $('a[href$="#LegalApproach"]');
    this.btnHighlights = $('a[href$="#CareerHighlights"]');
    this.btnEarlyYears = $('a[href$="#EarlyYears"]');
    this.btnEducation = $('a[href$="#Education"]');
    this.events();
  }

  events() {
    this.btnBiography.on('click', this.openBiography.bind(this));
    this.btnApproach.on('click', this.openApproach.bind(this));
    this.btnHighlights.on('click', this.openHighlights.bind(this));
    this.btnEarlyYears.on('click', this.openEarlyYears.bind(this));
    this.btnEducation.on('click', this.openEducation.bind(this));
  }

  // methods
  openBiography(e) {
    e.preventDefault();
    this.base.find('.active').removeClass('active');
    this.biography.addClass('active');
  }

  openApproach(e) {
    e.preventDefault();
    this.base.find('.active').removeClass('active');
    this.bioApproach.addClass('active');
  }

  openHighlights(e) {
    e.preventDefault();
    this.base.find('.active').removeClass('active');
    this.bioHighlights.addClass('active');
  }

  openEarlyYears(e) {
    e.preventDefault();
    this.base.find('.active').removeClass('active');
    this.bioEarlyYears.addClass('active');
  }

  openEducation(e) {
    e.preventDefault();
    this.base.find('.active').removeClass('active');
    this.bioEducation.addClass('active');
  }
}

export default Attorney;
