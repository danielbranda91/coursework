import $ from 'jquery';

class Search {
  constructor() {
    this.addSearchHTML();
    this.openButton = $('.search-trigger');
    this.closeButton = $('.search-overlay__close');
    this.searchOverlay = $('.search-overlay');
    this.resultsDiv = $('#search-overlay__results');
    this.searchField = $('#search-term');
    this.events();
    this.isOverLayOpen = false;
    this.isSpinnerVisible = false;
    this.previousValue;
    this.typingTimer;
  }

  events() {
    this.openButton.on('click', this.openOverlay.bind(this));
    this.closeButton.on('click', this.closeOverlay.bind(this));
    $(document).on('keydown', this.keyPressDispatcher.bind(this));
    this.searchField.on('keyup', this.typingLogic.bind(this));
  }

  // methods
  typingLogic() {
    if (this.searchField.val() != this.previousValue) {
      clearTimeout(this.typingTimer);
      if (this.searchField.val()) {
        if (!this.isSpinnerVisible) {
          this.resultsDiv.html('<div class="spinner-loader"></div>');
          this.isSpinnerVisible = true;
        }
      }
      this.typingTimer = setTimeout(this.getResults.bind(this), 750);
    } else {
      this.resultsDiv.html('');
      this.isSpinnerVisible = false;
    }
    this.previousValue = this.searchField.val();
  }

  getResults() {
    $.getJSON(
      lawfirmData.root_url +
        '/wp-json/wp/v2/posts?search=' +
        this.searchField.val(),
      posts => {
        this.resultsDiv.html(`
          <h2 class="search-overlay__section-title">General Information</h2>
            ${
              posts.length
                ? '<ul>'
                : '<p>No general information matches that search</p>'
            }
              ${posts
                .map(
                  item =>
                    `<li><a href="${item.link}">${item.title.rendered}</a></li>`
                )
                .join('')}
            ${posts.length ? '</ul>' : ''}
        `);
      }
    );
    this.isSpinnerVisible = false;
  }

  keyPressDispatcher(e) {
    if (
      e.keyCode == 83 &&
      !this.isOverLayOpen &&
      !$('input, textarea').is(':focus')
    ) {
      this.openOverlay();
    }
    if (e.keyCode == 27 && this.isOverLayOpen) {
      this.closeOverlay();
    }
  }

  openOverlay() {
    this.searchOverlay.addClass('search-overlay--active');
    $('body').addClass('body-no-scroll');
    this.searchField.val('');
    setTimeout(() => this.searchField.focus(), 301);
    this.isOverLayOpen = true;
  }

  closeOverlay() {
    this.searchOverlay.removeClass('search-overlay--active');
    $('body').removeClass('body-no-scroll');
    this.isOverLayOpen = false;
  }

  addSearchHTML() {
    $('body').append(`
      <div class="search-overlay">
        <div class="search-overlay__top">
          <div class="full">
            <i class="fas fa-search search-overlay__icon" aria-hidden="true"></i>
            <input type="text" class="search-term" placeholder="What are you looking for?" id="search-term">
            <i class="fas fa-window-close search-overlay__close" aria-hidden="true"></i>
          </div>
        </div>
        <div class="container">
          <div id="search-overlay__results"></div>
        </div>
      </div>    
    `);
  }
}

export default Search;
