import $ from 'jquery';

class MobileMenu {
  constructor() {
    this.openButton = $('.menu-trigger');
    this.closeButton = $('.exit');
    this.topNav = $('.top-nav');
    this.isParent = $('#topMobileMenu'); // add .leave to slide
    this.hasChild = $('.hasChild');
    this.childMenu = $('.childMenu');
    this.events();
  }

  events() {
    this.openButton.on('click', this.openNavMenu.bind(this));
    this.closeButton.on('click', this.closeNavMenu.bind(this));
    this.hasChild.on('click', this.openChild.bind(this));
    this.childMenu.on('click', this.closeChild.bind(this));
  }

  // methods
  openNavMenu() {
    this.topNav.addClass('active');
  }

  closeNavMenu() {
    this.topNav.removeClass('active');
    this.isParent.removeClass('leave');
    this.hasChild.removeClass('enter');
  }

  // try to be more precise with e.target.children(".selector") or e.target.parent(".selector")

  openChild(e) {
    var thisChild = $(e.target);
    thisChild.addClass('enter');
    thisChild.parent('#topMobileMenu').addClass('leave');
  }

  closeChild(e) {
    event.stopPropagation();
    // alert('Close child');
    this.hasChild.removeClass('enter');
    this.isParent.removeClass('leave');
  }
}

export default MobileMenu;
