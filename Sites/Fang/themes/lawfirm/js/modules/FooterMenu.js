import $ from 'jquery';

class FooterMenu {
  constructor() {
    this.openFootButton = $('.foot-links');
    this.events();
    this.isMenuOpen = false;
  }

  events() {
    this.openFootButton.on('click', this.toggleFooterMenu.bind(this));
  }

  toggleFooterMenu() {
    if (!this.isMenuOpen) {
      this.openFootButton.addClass('open');
      this.isMenuOpen = true;
    } else {
      this.openFootButton.removeClass('open');
      this.isMenuOpen = false;
    }
  }
}

export default FooterMenu;
