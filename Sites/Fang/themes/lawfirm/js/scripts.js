import $ from 'jquery';

// our modules and classes
import MobileMenu from './modules/MobileMenu';
import Search from './modules/Search';
import FooterMenu from './modules/FooterMenu';
import Attorney from './modules/Attorney';

// Instantiate a new object using modules
var mobileMenu = new MobileMenu();
var search = new Search();
var footerMenu = new FooterMenu();
var attorney = new Attorney();
