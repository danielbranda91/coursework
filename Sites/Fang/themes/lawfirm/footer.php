  <!-- Footer -->
  <footer>
    <section class="footer-section">
      <div>
        <div class="schema-link-section">
          <div class="box main">
            <ul class="schema">
              <li>
                <h4><?php echo get_bloginfo('description');?></h4>
                <span style="display:none"><?php echo get_bloginfo('name'); ?></span>
                <img src="https://via.placeholder.com/250" alt="Lawfirm" title="Lawfirm">
              </li>
              <li>
                <h3>
                  <a href="#" class="phone-link">555.555.5555</a>
                </h3>
              </li>
              <li>
                <span>
                  <span>1 Main Street, Suite 1000</span>
                  <br>
                  <span>New York</span>,
                  <span>NY</span>
                  <span>10001</span>
                </span>
                <span>
                  <br>
                  <a href="#">View Map [+]</a>
                </span>
              </li>
              <li>
                <span class="sitelink">
                  Website: <a href="index.html" class="schema-url" style="word-wrap:break-word;max-width:100%"><?php echo get_bloginfo('url');?></a>
                </span>
              </li>
            </ul>
            <div class="middle-foot">
              <div class="bottom-logo">
                <img src="https://via.placeholder.com/263x187" alt="Lawfirm" title="Lawfirm Footer Logo">
              </div>
              <nav class="soc-links">
                <ul class="social box">
                  <li class="social-button" data-item="i">
                    <a href="#" target="_blank" rel="nofollow">
                      <i class="fab fa-facebook-f"></i>&nbsp;
                    </a>
                  </li>
                  <li class="social-button" data-item="i">
                    <a href="#" target="_blank" rel="nofollow">
                      <i class="fab fa-google-plus-g"></i>&nbsp;
                    </a>
                  </li>
                  <li class="social-button" data-item="i">
                    <a href="#" target="_blank" rel="nofollow">
                      <i class="fab fa-twitter"></i>&nbsp;
                    </a>
                  </li>
                  <li class="social-button" data-item="i">
                    <a href="#" target="_blank" rel="nofollow">
                      <i class="fab fa-linkedin-in"></i>&nbsp;
                    </a>
                  </li>
                  <li class="social-button" data-item="i">
                    <a href="#" target="_blank" rel="nofollow">
                      <i class="fab fa-youtube"></i>
                    </a>
                  </li>
                </ul>
              </nav>
            </div>
            <nav class="foot-links">
              <header>
                <h3>Quick Links</h3>
              </header>
              <?php 
                wp_nav_menu(array(
                  'theme_location' => 'footerMenuLocation'
                ));
              ?>
            </nav>
          </div>
        </div>
        <div class="sd-footer">
          <div class="main box">
            <span class="scorp"></span>
            <small>
              <div class="copyright">
                <span><?php echo get_bloginfo('name'); ?></span>
                &copy; All Rights Reserved
              </div>
              The information on this website is for general information purposes only. Nothing on this site should be taken as legal advice for any individual case or situation. This information is not intended to create, and receipt or viewing does not constitute, an attorney-client relationship.
            </small>
          </div>
        </div>
      </div>
    </section>
  </footer>
  <?php 
   wp_footer(); 
  ?>
  </body>
</html>