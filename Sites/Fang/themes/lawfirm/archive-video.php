<?php get_header(); ?>
<!-- Body -->
<main>
  <section class="subblank-cont">
      <div class="main wide content-panel">
        <h1>Video Center</h1>
      </div>
    </section>
  <section class="main staff-list">
    <div class="atty-list">
      <header class="fancy main short">
        <h3>
          <strong>In The Media</strong>
        </h3>
      </header>
    </div>
    <div class="box-list attorneys">
      <ul>
        <?php 
          while(have_posts()) {
            the_post(); ?>
            <li class="item1">
              <div class="pic-box">
                <?php print_r(get_field('video_upload')); ?>
              </div>
              <div class="info-btn">
                <strong><?php the_title(); ?></strong>
              </div>
            </li>
          <?php }
        ?>
      </ul>
      <?php echo paginate_links(); ?>
    </div>
  </section>
    
  <?php get_template_part('template-parts/contact'); ?>
</main>
<?php get_footer(); ?>