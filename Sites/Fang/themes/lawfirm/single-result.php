<?php

  get_header();

  while (have_posts()) {
    the_post(); ?>

    <main>
      <section class="res-item info content-panel">
        <div class="main short">
          <header class="fancy" style="background-image:url(<?php echo get_the_post_thumbnail_url(get_the_ID(),'singleHeader'); ?>)">
            <h1><?php the_title(); ?></h1>
          </header>
          <div class="data">
            <h3 class="value"><strong>Case Value:</strong> <?php echo get_field('case_settlement');?></h3>

            <span class="case-deets">
              <?php the_content(); ?>
            </span>

            <div class="attys">
              <strong>Attorneys: </strong><span class="case-attys">#List Attorneys</span>
            </div>
            <div class="btn-container">
              <a href="<?php echo site_url('/case-results'); ?>" class="btn">
              Back to Case Results
              </a>
            </div>
          </div>
        </div>
      </section>
      <?php get_template_part('template-parts/contact'); ?>
    </main>
    

  <?php } 

get_footer(); ?>