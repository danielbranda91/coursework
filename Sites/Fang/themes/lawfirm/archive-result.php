<?php get_header(); ?>
<!-- Body -->
<main>
<section class="archive">
  <div class="main wide content-panel">
    <h1>Case Results</h1>
  </div>
</section>
<section class="main">
  <div class="bloghome">
  <?php 
    while (have_posts()) {
      the_post(); ?>
      <div class="item blog">
        <div class="dateline">
          <?php the_time('M d, Y'); ?>
        </div>
        <h2>
          <a href="<?php the_permalink(); ?>" class="vertical-align:top"><?php the_title(); ?></a>
        </h2>
        <div class="content content-panel">
          <span><?php echo wp_trim_words(get_the_content(),20); ?></span>
        </div>
        <div class="byline">
          <div>
            <span>Settlement: </span> <?php echo get_field('case_settlement'); ?>
          </div>
          <a href="<?php the_permalink(); ?>">Continue Reading &raquo;</a>
        </div>
        <div class="divider"></div>
        <div style="clear:both"></div>
      </div>

    <?php } 
    echo paginate_links();
  ?>
  </div>
</section>
    
<?php get_template_part('template-parts/contact'); ?>
</main>
<?php get_footer(); ?>
