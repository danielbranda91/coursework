<?php

function lawfirm_post_types() {
  register_post_type('attorney', array(
    'supports' => array('title','thumbnail', 'page-attributes'),
    'rewrite' => array('slug' => 'attorneys'),
    'public' => true,
    'labels' => array(
      'name' => 'Attorneys',
      'add_new_item' => 'Add New Attorney',
      'edit_item' => 'Edit Attorney',
      'all_items' => 'All Attorneys',
      'singular_name' => 'Attorney'
    ),
    'menu_icon' => 'dashicons-businessman'
  ));
  register_post_type('employee', array(
    'supports' => array('title','thumbnail'),
    'rewrite' => array('slug' => 'employees'),
    'public' => true,
    'labels' => array(
      'name' => 'Employees',
      'add_new_item' => 'Add New Employee',
      'edit_item' => 'Edit Employee',
      'all_items' => 'All Employees',
      'singular_name' => 'Employee'
    ),
    'menu_icon' => 'dashicons-businessperson'
  ));
  register_post_type('result', array(
    'supports' => array('title','editor','thumbnail'),
    'rewrite' => array('slug' => 'case-results'),
    'public' => true,
    'has_archive' => true,
    'labels' => array(
      'name' => 'Case Results',
      'add_new_item' => 'Add New Case Result',
      'edit_item' => 'Edit Case Result',
      'all_items' => 'All Case Results',
      'singular_name' => 'Case Result'
    ),
    'menu_icon' => 'dashicons-chart-line'
  ));
  register_post_type('testimonial', array(
    'supports' => array('title','editor','thumbnail'),
    'rewrite' => array('slug' => 'testimonials'),
    'public' => true,
    'labels' => array(
      'name' => 'Testimonials',
      'add_new_item' => 'Add New Testimonial',
      'edit_item' => 'Edit Testimonial',
      'all_items' => 'All Testimonials',
      'singular_name' => 'Testimonial'
    ),
    'menu_icon' => 'dashicons-megaphone'
  ));
  register_post_type('award', array(
    'supports' => array('title','editor','thumbnail'),
    'rewrite' => array('slug' => 'awards'),
    'public' => true,
    'labels' => array(
      'name' => 'Awards',
      'add_new_item' => 'Add New Award',
      'edit_item' => 'Edit Award',
      'all_items' => 'All Awards',
      'singular_name' => 'Award'
    ),
    'menu_icon' => 'dashicons-awards'
  ));
  register_post_type('video', array(
    'supports' => array('title','editor','thumbnail'),
    'rewrite' => array('slug' => 'videos'),
    'public' => true,
    'has_archive' => true,
    'labels' => array(
      'name' => 'Videos',
      'add_new_item' => 'Add New Video',
      'edit_item' => 'Edit Video',
      'all_items' => 'All Videos',
      'singular_name' => 'Video'
    ),
    'menu_icon' => 'dashicons-editor-video'
  ));
  register_post_type('practice', array(
    'supports' => array('title','editor','thumbnail','page-attributes'),
    'rewrite' => array('slug' => 'practice-areas'),
    'hierarchical' => true,
    'public' => true,
    'labels' => array(
      'name' => 'Practice Areas',
      'add_new_item' => 'Add New Practice Area',
      'edit_item' => 'Edit Practice Area',
      'all_items' => 'All Practice Areas',
      'singular_name' => 'Practice Area'
    ),
    'menu_icon' => 'dashicons-clipboard'
  ));
}

add_action('init', 'lawfirm_post_types');


